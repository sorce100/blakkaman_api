<?php
    require_once("../Model/db/DbConnect.php");

    class Company {
        private $company_id;
        private $company_category_id;
        private $company_name;
        private $company_manager_fname;
        private $company_manager_lname;
        private $company_tel;
        private $company_email;
        private $company_website;
        private $company_meta_keywords;
        private $company_meta_description;
        private $company_commission;
        private $company_region_id;
        private $company_address;
        private $company_bank;
        private $company_bank_branche;
        private $company_bank_account;
        private $company_folder;
        private $company_logo;
        private $company_notes;
        private $company_record_hide;
        private $company_block_status;
        private $company_created_date;
        private $company_updated_date;
        private $tableName = 'pos_mgt.companys';
        private $dbConn;

        function set_company_id($company_id) { $this->company_id = $company_id; }
        function set_company_category_id($company_category_id) { $this->company_category_id = $company_category_id; }
        function set_company_name($company_name) { $this->company_name = $company_name; }
        function set_company_manager_fname($company_manager_fname) { $this->company_manager_fname = $company_manager_fname; }
        function set_company_manager_lname($company_manager_lname) { $this->company_manager_lname = $company_manager_lname; }
        function set_company_tel($company_tel) { $this->company_tel = $company_tel; }
        function set_company_email($company_email) { $this->company_email = $company_email; }
        function set_company_website($company_website) { $this->company_website = $company_website; }
        function set_company_meta_keywords($company_meta_keywords) { $this->company_meta_keywords = $company_meta_keywords; }
        function set_company_meta_description($company_meta_description) { $this->company_meta_description = $company_meta_description; }
        function set_company_commission($company_commission) { $this->company_commission = $company_commission; }
        function set_company_region_id($company_region_id) { $this->company_region_id = $company_region_id; }
        function set_company_address($company_address) { $this->company_address = $company_address; }
        function set_company_bank($company_bank) { $this->company_bank = $company_bank; }
        function set_company_bank_branche($company_bank_branche) { $this->company_bank_branche = $company_bank_branche; }
        function set_company_bank_account($company_bank_account) { $this->company_bank_account = $company_bank_account; }
        function set_company_folder($company_folder) { $this->company_folder = $company_folder; }
        function set_company_logo($company_logo) { $this->company_logo = $company_logo; }
        function set_company_notes($company_notes) { $this->company_notes = $company_notes; }
        function set_company_record_hide($company_record_hide) { $this->company_record_hide = $company_record_hide; }
        function set_company_block_status($company_block_status) { $this->company_block_status = $company_block_status; }
        function set_company_created_date($company_created_date) { $this->company_created_date = $company_created_date; }
        function set_company_updated_date($company_updated_date) { $this->company_updated_date = $company_updated_date; }

        public function __construct() {
			$db = new DbConnect();
			$this->dbConn = $db->connect();
        }
        
        // get all 
        public function get_all() {
            $stmt = $this->dbConn->prepare("SELECT * FROM $this->tableName WHERE company_record_hide=:recordHide");
            $stmt->bindParam(':recordHide', $this->company_record_hide);
			$stmt->execute();
			$response = $stmt->fetchAll(PDO::FETCH_ASSOC);
			return $response;
        }
        
        // get  by id
        public function get_by_id(){
            $stmt = $this->dbConn->prepare("SELECT * FROM $this->tableName WHERE company_id=:id AND company_record_hide = :recordHide");
            $stmt->bindParam(':id', $this->company_id);
            $stmt->bindParam(':recordHide', $this->company_record_hide);
			$stmt->execute();
			$response = $stmt->fetch(PDO::FETCH_ASSOC);
			return $response;
        }
        // insert company
        public function insert(){
            $sql = "INSERT INTO $this->tableName(
                company_category_id,
                company_name,
                company_manager_fname,
                company_manager_lname,
                company_tel,
                company_email,
                company_website,
                company_meta_keywords,
                company_meta_description,
                company_commission,
                company_region_id,
                company_address,
                company_bank,
                company_bank_branche,
                company_bank_account,
                company_folder,
                company_logo,
                company_notes,
                company_record_hide,
                company_block_status,
                company_created_date,
                company_updated_date)
            VALUES (
                :companyCategory,
                :companyName,
                :companyManagerFname,
                :companyManagerLname,
                :companyTel,
                :companyEmail,
                :companyWebsite,
                :companyMetaKeywords,
                :companyMetaDescription,
                :companyCommssion,
                :companyRegionId,
                :companyAddress,
                :companyBank,
                :companyBankBranche,
                :companyBankAccount,
                :companyFolder,
                :companyLogo,
                :companyNotes,
                :companyRecordHide,
                :companyBlockStatus,
                :companyCreatedDate,
                :companyUpdatedDate)";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(':companyCategory', $this->company_category_id);
            $stmt->bindParam(':companyName', $this->company_name);
            $stmt->bindParam(':companyManagerFname', $this->company_manager_fname);
            $stmt->bindParam(':companyManagerLname', $this->company_manager_lname);
            $stmt->bindParam(':companyTel', $this->company_tel);
            $stmt->bindParam(':companyEmail', $this->company_email);
            $stmt->bindParam(':companyWebsite', $this->company_website);
            $stmt->bindParam(':companyMetaKeywords', $this->company_meta_keywords);
            $stmt->bindParam(':companyMetaDescription', $this->company_meta_description);
            $stmt->bindParam(':companyCommssion', $this->company_commission);
            $stmt->bindParam(':companyRegionId', $this->company_region_id);
            $stmt->bindParam(':companyAddress', $this->company_address);
            $stmt->bindParam(':companyBank', $this->company_bank);
            $stmt->bindParam(':companyBankBranche', $this->company_bank_branche);
            $stmt->bindParam(':companyBankAccount', $this->company_bank_account);
            $stmt->bindParam(':companyFolder', $this->company_folder);
            $stmt->bindParam(':companyLogo', $this->company_logo);
            $stmt->bindParam(':companyNotes', $this->company_notes);
            $stmt->bindParam(':companyRecordHide', $this->company_record_hide);
            $stmt->bindParam(':companyBlockStatus', $this->company_block_status);
            $stmt->bindParam(':companyCreatedDate', $this->company_created_date);
            $stmt->bindParam(':companyUpdatedDate', $this->company_updated_date);
			if($stmt->execute()) {
				return true;
			} else {
				return false;
			}
        }
        // update
        public function update(){
            $sql = "UPDATE $this->tableName SET 
            company_category_id=:companyCategory,
            company_name=:companyName,
            company_manager_fname=:companyManagerFname,
            company_manager_lname=:companyManagerLname,
            company_tel=:companyTel,
            company_email=:companyEmail,
            company_website=:companyWebsite,
            company_meta_keywords=:companyMetaKeywords,
            company_meta_description=:companyMetaDescription,
            company_commission=:companyCommssion,
            company_region_id=:companyRegionId,
            company_address=:companyAddress,
            company_bank=:companyBank,
            company_bank_branche=:companyBankBranche,
            company_bank_account=:companyBankAccount,
            company_logo=:companyLogo,
            company_notes=:companyNotes,
            company_updated_date=:companyUpdatedDate
            WHERE company_id=:id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(':companyCategory', $this->company_category_id);
            $stmt->bindParam(':companyName', $this->company_name);
            $stmt->bindParam(':companyManagerFname', $this->company_manager_fname);
            $stmt->bindParam(':companyManagerLname', $this->company_manager_lname);
            $stmt->bindParam(':companyTel', $this->company_tel);
            $stmt->bindParam(':companyEmail', $this->company_email);
            $stmt->bindParam(':companyWebsite', $this->company_website);
            $stmt->bindParam(':companyMetaKeywords', $this->company_meta_keywords);
            $stmt->bindParam(':companyMetaDescription', $this->company_meta_description);
            $stmt->bindParam(':companyCommssion', $this->company_commission);
            $stmt->bindParam(':companyRegionId', $this->company_region_id);
            $stmt->bindParam(':companyAddress', $this->company_address);
            $stmt->bindParam(':companyBank', $this->company_bank);
            $stmt->bindParam(':companyBankBranche', $this->company_bank_branche);
            $stmt->bindParam(':companyBankAccount', $this->company_bank_account);
            $stmt->bindParam(':companyLogo', $this->company_logo);
            $stmt->bindParam(':companyNotes', $this->company_notes);
            $stmt->bindParam(':companyUpdatedDate', $this->company_updated_date);
			$stmt->bindParam(':id', $this->company_id);
			if($stmt->execute()) {
				return true;
			} else {
				return false;
			}
        }
        // delete
        public function delete(){
            $sql = "UPDATE $this->tableName SET 
            company_record_hide=:recordHide,
            company_updated_date=:updateDate
            WHERE company_id=:id";
			$stmt = $this->dbConn->prepare($sql);
            $stmt->bindParam(':recordHide', $this->company_record_hide);
            $stmt->bindParam(':updateDate', $this->company_updated_date);
			$stmt->bindParam(':id', $this->company_id);
			if($stmt->execute()) {
				return true;
			} else {
				return false;
			}
        }
        // block status
        public function block_status(){
            $sql = "UPDATE $this->tableName SET 
            company_block_status=:blockStatus,
            company_updated_date=:updateDate
            WHERE company_id=:id";
			$stmt = $this->dbConn->prepare($sql);
            $stmt->bindParam(':blockStatus', $this->company_block_status);
            $stmt->bindParam(':updateDate', $this->company_updated_date);
			$stmt->bindParam(':id', $this->company_id);
			if($stmt->execute()) {
				return true;
			} else {
				return false;
			}
        }
    }


?>