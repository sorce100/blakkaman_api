<?php
    require_once("../Model/db/DbConnect.php");

    class Branche {
        
        private $branche_id;
        private $branche_company_id;
        private $branche_name;
        private $branche_location;
        private $branche_address;
        private $branche_contact_fname;
        private $branche_contact_lname;
        private $branche_tel;
        private $branche_ghana_post;
        private $branche_longitude;
        private $branche_latitude;
        private $branche_region_id;
        private $branche_notes;
        private $branche_record_hide;
        private $branche_block_status;
        private $branche_created_date;
        private $branche_updated_date;
        private $tableName = 'pos_mgt.branches';
        private $dbConn;

        function set_branche_id($branche_id) { $this->branche_id = $branche_id; }
        function set_branche_company_id($branche_company_id) { $this->branche_company_id = $branche_company_id; }
        function set_branche_name($branche_name) { $this->branche_name = $branche_name; }
        function set_branche_location($branche_location) { $this->branche_location = $branche_location; }
        function set_branche_address($branche_address) { $this->branche_address = $branche_address; }
        function set_branche_contact_fname($branche_contact_fname) { $this->branche_contact_fname = $branche_contact_fname; }
        function set_branche_contact_lname($branche_contact_lname) { $this->branche_contact_lname = $branche_contact_lname; }
        function set_branche_tel($branche_tel) { $this->branche_tel = $branche_tel; }
        function set_branche_ghana_post($branche_ghana_post) { $this->branche_ghana_post = $branche_ghana_post; }
        function set_branche_longitude($branche_longitude) { $this->branche_longitude = $branche_longitude; }
        function set_branche_latitude($branche_latitude) { $this->branche_latitude = $branche_latitude; }
        function set_branche_region_id($branche_region_id) { $this->branche_region_id = $branche_region_id; }
        function set_branche_notes($branche_notes) { $this->branche_notes = $branche_notes; }
        function set_branche_record_hide($branche_record_hide) { $this->branche_record_hide = $branche_record_hide; }
        function set_branche_block_status($branche_block_status) { $this->branche_block_status = $branche_block_status; }
        function set_branche_created_date($branche_created_date) { $this->branche_created_date = $branche_created_date; }
        function set_branche_updated_date($branche_updated_date) { $this->branche_updated_date = $branche_updated_date; }

        public function __construct() {
			$db = new DbConnect();
			$this->dbConn = $db->connect();
        }
        // get all 
        public function get_all() {
            $stmt = $this->dbConn->prepare("SELECT B.*, R.region_name ,C.company_name FROM $this->tableName AS B
                                            LEFT JOIN pos_mgt.regions AS R ON
                                            B.branche_region_id = R.region_id
                                            LEFT JOIN pos_mgt.companys AS C ON
                                            B.branche_company_id = C.company_id
                                            WHERE branche_record_hide=:recordHide
                                            ORDER BY branche_id DESC"
                                        );
            $stmt->bindParam(':recordHide', $this->branche_record_hide);
			$stmt->execute();
			$response = $stmt->fetchAll(PDO::FETCH_ASSOC);
			return $response;
        }
        
        // get by id
        public function get_by_id(){
            $stmt = $this->dbConn->prepare("SELECT * FROM $this->tableName WHERE branche_id=:id AND branche_record_hide = :recordHide");
            $stmt->bindParam(':id', $this->branche_id);
            $stmt->bindParam(':recordHide', $this->branche_record_hide);
			$stmt->execute();
			$response = $stmt->fetch(PDO::FETCH_ASSOC);
			return $response;
        }
        // insert 
        public function insert(){
            $sql = "INSERT INTO $this->tableName(
                branche_company_id,
                branche_name,
                branche_location,
                branche_address,
                branche_contact_fname,
                branche_contact_lname,
                branche_tel,
                branche_ghana_post,
                branche_longitude,
                branche_latitude,
                branche_region_id,
                branche_notes,
                branche_record_hide,
                branche_block_status,
                branche_created_date,
                branche_updated_date
                )
            VALUES (
                :companyId,
                :brancheName,
                :brancheLocation,
                :brancheAddress,
                :brancheFname,
                :brancheLname,
                :brancheTel,
                :brancheGhPost,
                :brancheLong,
                :brancheLat,
                :brancheRegId,
                :brancheNotes,
                :recordHide,
                :blockStatus,
                :createdDate,
                :updateDate
                )";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(':companyId', $this->branche_company_id);
			$stmt->bindParam(':brancheName', $this->branche_name);
			$stmt->bindParam(':brancheLocation', $this->branche_location);
			$stmt->bindParam(':brancheAddress', $this->branche_address);
			$stmt->bindParam(':brancheFname', $this->branche_contact_fname);
			$stmt->bindParam(':brancheLname', $this->branche_contact_lname);
			$stmt->bindParam(':brancheTel', $this->branche_tel);
			$stmt->bindParam(':brancheGhPost', $this->branche_ghana_post);
			$stmt->bindParam(':brancheLong', $this->branche_longitude);
			$stmt->bindParam(':brancheLat', $this->branche_latitude);
			$stmt->bindParam(':brancheRegId', $this->branche_region_id);
			$stmt->bindParam(':brancheNotes', $this->branche_notes);
			$stmt->bindParam(':recordHide', $this->branche_record_hide);
			$stmt->bindParam(':blockStatus', $this->branche_block_status);
			$stmt->bindParam(':createdDate', $this->branche_created_date);
			$stmt->bindParam(':updateDate', $this->branche_updated_date);
			if($stmt->execute()) {
				return true;
			} else {
				return false;
			}
        }
        // update
        public function update(){
            $sql = "UPDATE $this->tableName SET 
            branche_company_id = :companyId,
            branche_name = :brancheName,
            branche_location = :brancheLocation,
            branche_address = :brancheAddress,
            branche_contact_fname = :brancheFname,
            branche_contact_lname = :brancheLname,
            branche_tel = :brancheTel,
            branche_ghana_post = :brancheGhPost,
            branche_longitude = :brancheLong,
            branche_latitude = :brancheLat,
            branche_region_id = :brancheRegId,
            branche_notes = :brancheNotes,
            branche_updated_date = :updateDate
            WHERE branche_id=:id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(':companyId', $this->branche_company_id);
			$stmt->bindParam(':brancheName', $this->branche_name);
			$stmt->bindParam(':brancheLocation', $this->branche_location);
			$stmt->bindParam(':brancheAddress', $this->branche_address);
			$stmt->bindParam(':brancheFname', $this->branche_contact_fname);
			$stmt->bindParam(':brancheLname', $this->branche_contact_lname);
			$stmt->bindParam(':brancheTel', $this->branche_tel);
			$stmt->bindParam(':brancheGhPost', $this->branche_ghana_post);
			$stmt->bindParam(':brancheLong', $this->branche_longitude);
			$stmt->bindParam(':brancheLat', $this->branche_latitude);
			$stmt->bindParam(':brancheRegId', $this->branche_region_id);
			$stmt->bindParam(':brancheNotes', $this->branche_notes);
			$stmt->bindParam(':updateDate', $this->branche_updated_date);
			$stmt->bindParam(':id', $this->branche_id);
			if($stmt->execute()) {
				return true;
			} else {
				return false;
			}
        }
        // delete
        public function delete(){
            $sql = "UPDATE $this->tableName SET 
            branche_record_hide=:recordHide,
            branche_updated_date=:updateDate 
            WHERE branche_id=:id";
			$stmt = $this->dbConn->prepare($sql);
            $stmt->bindParam(':recordHide', $this->branche_record_hide);
            $stmt->bindParam(':updateDate', $this->branche_updated_date);
			$stmt->bindParam(':id', $this->branche_id);
			if($stmt->execute()) {
				return true;
			} else {
				return false;
			}
        }
        // block status
        public function block_status(){
            $sql = "UPDATE $this->tableName SET 
            branche_block_status=:blockStatus,
            branche_updated_date=:updateDate 
            WHERE branche_id=:id";
			$stmt = $this->dbConn->prepare($sql);
            $stmt->bindParam(':blockStatus', $this->branche_block_status);
            $stmt->bindParam(':updateDate', $this->branche_updated_date);
			$stmt->bindParam(':id', $this->branche_id);
			if($stmt->execute()) {
				return true;
			} else {
				return false;
			}
        }
        
    }


?>