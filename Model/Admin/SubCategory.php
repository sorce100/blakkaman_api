<?php
    require_once("../Model/db/DbConnect.php");

    class SubCategory {
        
        private $sub_category_id;
        private $sub_main_category_id;
        private $sub_category_name;
        private $sub_category_record_hide;
        private $sub_category_block_status;
        private $sub_category_created_date;
        private $sub_category_updated_date;
        private $tableName = 'pos_mgt.sub_categories';
        private $dbConn;

        function set_sub_category_id($sub_category_id) { $this->sub_category_id = $sub_category_id; }
        function set_sub_main_category_id($sub_main_category_id) { $this->sub_main_category_id = $sub_main_category_id; }
        function set_sub_category_name($sub_category_name) { $this->sub_category_name = $sub_category_name; }
        function set_sub_category_record_hide($sub_category_record_hide) { $this->sub_category_record_hide = $sub_category_record_hide; }
        function set_sub_category_block_status($sub_category_block_status) { $this->sub_category_block_status = $sub_category_block_status; }
        function set_sub_category_created_date($sub_category_created_date) { $this->sub_category_created_date = $sub_category_created_date; }
        function set_sub_category_updated_date($sub_category_updated_date) { $this->sub_category_updated_date = $sub_category_updated_date; }

        public function __construct() {
			$db = new DbConnect();
			$this->dbConn = $db->connect();
        }
        
        // get all 
        public function get_all() {
            $stmt = $this->dbConn->prepare("SELECT SC.*, C.category_name FROM $this->tableName AS SC
                                            LEFT JOIN pos_mgt.categories AS C ON
                                            SC.sub_main_category_id = C.category_id
                                            WHERE sub_category_record_hide=:recordHide
                                            ORDER BY sub_category_id DESC"
                                        );
            $stmt->bindParam(':recordHide', $this->sub_category_record_hide);
			$stmt->execute();
			$response = $stmt->fetchAll(PDO::FETCH_ASSOC);
			return $response;
        }
        
        // get by id
        public function get_by_id(){
            $stmt = $this->dbConn->prepare("SELECT * FROM $this->tableName WHERE sub_category_id=:id AND sub_category_record_hide = :recordHide");
            $stmt->bindParam(':id', $this->sub_category_id);
            $stmt->bindParam(':recordHide', $this->sub_category_record_hide);
			$stmt->execute();
			$response = $stmt->fetch(PDO::FETCH_ASSOC);
			return $response;
        }
        // insert 
        public function insert(){
            $sql = "INSERT INTO $this->tableName(sub_main_category_id, sub_category_name, sub_category_record_hide, sub_category_block_status, sub_category_created_date, sub_category_updated_date)
            VALUES (:mainCategoryId, :subCategoryName, :recordHide, :blockStatus, :createdDate, :updateDate)";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(':mainCategoryId', $this->sub_main_category_id);
			$stmt->bindParam(':subCategoryName', $this->sub_category_name);
			$stmt->bindParam(':recordHide', $this->sub_category_record_hide);
			$stmt->bindParam(':blockStatus', $this->sub_category_block_status);
			$stmt->bindParam(':createdDate', $this->sub_category_created_date);
			$stmt->bindParam(':updateDate', $this->sub_category_updated_date);
			if($stmt->execute()) {
				return true;
			} else {
				return false;
			}
        }
        // update
        public function update(){
            $sql = "UPDATE $this->tableName SET 
            sub_main_category_id=:mainCategoryId,
            sub_category_name=:subCategoryName,
            sub_category_updated_date=:updateDate 
            WHERE sub_category_id=:id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(':mainCategoryId', $this->sub_main_category_id);
			$stmt->bindParam(':subCategoryName', $this->sub_category_name);
			$stmt->bindParam(':updateDate', $this->sub_category_updated_date);
			$stmt->bindParam(':id', $this->sub_category_id);
			if($stmt->execute()) {
				return true;
			} else {
				return false;
			}
        }
        // delete
        public function delete(){
            $sql = "UPDATE $this->tableName SET 
            sub_category_record_hide=:recordHide,
            sub_category_updated_date=:updateDate 
            WHERE sub_category_id=:id";
			$stmt = $this->dbConn->prepare($sql);
            $stmt->bindParam(':recordHide', $this->sub_category_record_hide);
            $stmt->bindParam(':updateDate', $this->sub_category_updated_date);
			$stmt->bindParam(':id', $this->sub_category_id);
			if($stmt->execute()) {
				return true;
			} else {
				return false;
			}
        }
        // block status
        public function block_status(){
            $sql = "UPDATE $this->tableName SET 
            sub_category_block_status=:blockStatus,
            sub_category_updated_date=:updateDate 
            WHERE sub_category_id=:id";
			$stmt = $this->dbConn->prepare($sql);
            $stmt->bindParam(':blockStatus', $this->sub_category_block_status);
            $stmt->bindParam(':updateDate', $this->sub_category_updated_date);
			$stmt->bindParam(':id', $this->sub_category_id);
			if($stmt->execute()) {
				return true;
			} else {
				return false;
			}
        }
        
    }


?>