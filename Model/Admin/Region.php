<?php
    require_once("../Model/db/DbConnect.php");

    class Region {
        private $region_id;
        private $region_name;
        private $region_record_hide;
        private $region_block_status;
        private $region_created_date;
        private $region_updated_date;
        private $tableName = 'pos_mgt.regions';
        private $dbConn;

        function set_region_id($region_id) { $this->region_id = $region_id; }
        function set_region_name($region_name) { $this->region_name = $region_name; }
        function set_region_record_hide($region_record_hide) { $this->region_record_hide = $region_record_hide; }
        function set_region_block_status($region_block_status) { $this->region_block_status = $region_block_status; }
        function set_region_created_date($region_created_date) { $this->region_created_date = $region_created_date; }
        function set_region_updated_date($region_updated_date) { $this->region_updated_date = $region_updated_date; }

        public function __construct() {
			$db = new DbConnect();
			$this->dbConn = $db->connect();
        }
        
        // get all 
        public function get_all() {
            $stmt = $this->dbConn->prepare("SELECT * FROM $this->tableName 
                                            WHERE region_record_hide=:recordHide
                                            ORDER BY region_id DESC"
                                        );
            $stmt->bindParam(':recordHide', $this->region_record_hide);
			$stmt->execute();
			$response = $stmt->fetchAll(PDO::FETCH_ASSOC);
			return $response;
        }
        
        // get  by id
        public function get_by_id(){
            $stmt = $this->dbConn->prepare("SELECT * FROM $this->tableName WHERE region_id=:id AND region_record_hide = :recordHide");
            $stmt->bindParam(':id', $this->region_id);
            $stmt->bindParam(':recordHide', $this->region_record_hide);
			$stmt->execute();
			$response = $stmt->fetch(PDO::FETCH_ASSOC);
			return $response;
        }
        // insert 
        public function insert(){
            $sql = "INSERT INTO $this->tableName( region_name, region_record_hide, region_block_status, region_created_date, region_updated_date)
            VALUES ( :regionName, :recordHide, :blockStatus, :createdDate, :updateDate)";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(':regionName', $this->region_name);
			$stmt->bindParam(':recordHide', $this->region_record_hide);
			$stmt->bindParam(':blockStatus', $this->region_block_status);
			$stmt->bindParam(':createdDate', $this->region_created_date);
			$stmt->bindParam(':updateDate', $this->region_updated_date);
			if($stmt->execute()) {
				return true;
			} else {
				return false;
			}
        }
        // update
        public function update(){
            $sql = "UPDATE $this->tableName SET 
            region_name=:regionName,
            region_updated_date=:updateDate 
            WHERE region_id=:id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(':regionName', $this->region_name);
			$stmt->bindParam(':updateDate', $this->region_updated_date);
			$stmt->bindParam(':id', $this->region_id);
			if($stmt->execute()) {
				return true;
			} else {
				return false;
			}
        }
        // delete
        public function delete(){
            $sql = "UPDATE $this->tableName SET 
            region_record_hide=:recordHide,
            region_updated_date=:updateDate 
            WHERE region_id=:id";
			$stmt = $this->dbConn->prepare($sql);
            $stmt->bindParam(':recordHide', $this->region_record_hide);
            $stmt->bindParam(':updateDate', $this->region_updated_date);
			$stmt->bindParam(':id', $this->region_id);
			if($stmt->execute()) {
				return true;
			} else {
				return false;
			}
        }
        // block status
        public function block_status(){
            $sql = "UPDATE $this->tableName SET 
            region_block_status=:blockStatus,
            region_updated_date=:updateDate 
            WHERE region_id=:id";
			$stmt = $this->dbConn->prepare($sql);
            $stmt->bindParam(':blockStatus', $this->region_block_status);
            $stmt->bindParam(':updateDate', $this->region_updated_date);
			$stmt->bindParam(':id', $this->region_id);
			if($stmt->execute()) {
				return true;
			} else {
				return false;
			}
        }
    }


?>