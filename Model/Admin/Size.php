<?php
    require_once("../Model/db/DbConnect.php");

    class Size {
        private $size_id;
        private $size_name;
        private $size_company_id;
        private $size_branche_id;
        private $size_record_hide;
        private $size_block_status;
        private $size_account_type;
        private $size_created_date;
        private $size_update_branche_id;
        private $size_updated_date;
        private $tableName = 'pos_mgt.sizes';
        private $dbConn;

        function set_size_id($size_id) { $this->size_id = $size_id; }
        function set_size_name($size_name) { $this->size_name = $size_name; }
        function set_size_company_id($size_company_id) { $this->size_company_id = $size_company_id; }
        function set_size_branche_id($size_branche_id) { $this->size_branche_id = $size_branche_id; }
        function set_size_record_hide($size_record_hide) { $this->size_record_hide = $size_record_hide; }
        function set_size_block_status($size_block_status) { $this->size_block_status = $size_block_status; }
        function set_size_account_type($size_account_type) { $this->size_account_type = $size_account_type; }
        function set_size_created_date($size_created_date) { $this->size_created_date = $size_created_date; }
        function set_size_update_branche_id($size_update_branche_id) { $this->size_update_branche_id = $size_update_branche_id; }
        function set_size_updated_date($size_updated_date) { $this->size_updated_date = $size_updated_date; }

        
        public function __construct() {
			$db = new DbConnect();
			$this->dbConn = $db->connect();
        }
        
        // get all sizes
        public function get_all() {
            $stmt = $this->dbConn->prepare("SELECT * FROM $this->tableName 
                                            WHERE size_company_id=:companyId 
                                            AND size_record_hide=:recordHide"
                                        );
            $stmt->bindParam(':companyId', $this->size_company_id);
            $stmt->bindParam(':recordHide', $this->size_record_hide);
			$stmt->execute();
			$response = $stmt->fetchAll(PDO::FETCH_ASSOC);
			return $response;
        }
        
        // get sizes by id
        public function get_by_id(){
            $stmt = $this->dbConn->prepare("SELECT * FROM $this->tableName WHERE size_id=:id AND size_record_hide = :recordHide");
            $stmt->bindParam(':id', $this->size_id);
            $stmt->bindParam(':recordHide', $this->size_record_hide);
			$stmt->execute();
			$response = $stmt->fetch(PDO::FETCH_ASSOC);
			return $response;
        }
        // insert sizes
        public function insert(){
            $sql = "INSERT INTO $this->tableName( size_name, size_company_id, size_branche_id, size_record_hide, size_block_status, size_account_type, size_created_date, size_update_branche_id, size_updated_date)
            VALUES ( :sizeName, :companyId, :brancheId, :recordHide, :blockStatus, :accountType, :createdDate, :updateBrancheId, :updateDate)";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(':sizeName', $this->size_name);
			$stmt->bindParam(':companyId', $this->size_company_id);
			$stmt->bindParam(':brancheId', $this->size_branche_id);
			$stmt->bindParam(':recordHide', $this->size_record_hide);
			$stmt->bindParam(':blockStatus', $this->size_block_status);
			$stmt->bindParam(':accountType', $this->size_account_type);
			$stmt->bindParam(':createdDate', $this->size_created_date);
			$stmt->bindParam(':updateBrancheId', $this->size_update_branche_id);
			$stmt->bindParam(':updateDate', $this->size_updated_date);
			if($stmt->execute()) {
				return true;
			} else {
				return false;
			}
        }
        // update
        public function update(){
            $sql = "UPDATE $this->tableName SET 
            size_name=:sizeName,
            size_update_branche_id=:updateBrancheId,
            size_updated_date=:updateDate 
            WHERE size_id=:id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(':sizeName', $this->size_name);
			$stmt->bindParam(':updateBrancheId', $this->size_update_branche_id);
			$stmt->bindParam(':updateDate', $this->size_updated_date);
			$stmt->bindParam(':id', $this->size_id);
			if($stmt->execute()) {
				return true;
			} else {
				return false;
			}
        }
        // delete
        public function delete(){
            $sql = "UPDATE $this->tableName SET 
            size_record_hide=:recordHide,
            size_updated_date=:updateDate,
            size_update_branche_id=:updateBrancheId
            WHERE size_id=:id";
			$stmt = $this->dbConn->prepare($sql);
            $stmt->bindParam(':recordHide', $this->size_record_hide);
            $stmt->bindParam(':updateDate', $this->size_updated_date);
            $stmt->bindParam(':updateBrancheId', $this->size_update_branche_id);
			$stmt->bindParam(':id', $this->size_id);
			if($stmt->execute()) {
				return true;
			} else {
				return false;
			}
        }
        // block status
        public function block_status(){
            $sql = "UPDATE $this->tableName SET 
            size_block_status=:blockStatus,
            size_updated_date=:updateDate,
            size_update_branche_id=:updateBrancheId
            WHERE size_id=:id";
			$stmt = $this->dbConn->prepare($sql);
            $stmt->bindParam(':blockStatus', $this->size_block_status);
            $stmt->bindParam(':updateDate', $this->size_updated_date);
            $stmt->bindParam(':updateBrancheId', $this->size_update_branche_id);
			$stmt->bindParam(':id', $this->size_id);
			if($stmt->execute()) {
				return true;
			} else {
				return false;
			}
        }
    }


?>