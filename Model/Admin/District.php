<?php
    require_once("../Model/db/DbConnect.php");

    class District {
        private $district_id;
        private $district_region_id;
        private $district_name;
        private $district_record_hide;
        private $district_block_status;
        private $district_created_date;
        private $district_updated_date;
        private $tableName = 'pos_mgt.districts';
        private $dbConn;

        function set_district_id($district_id) { $this->district_id = $district_id; }
        function set_district_region_id($district_region_id) { $this->district_region_id = $district_region_id; }
        function set_district_name($district_name) { $this->district_name = $district_name; }
        function set_district_record_hide($district_record_hide) { $this->district_record_hide = $district_record_hide; }
        function set_district_block_status($district_block_status) { $this->district_block_status = $district_block_status; }
        function set_district_created_date($district_created_date) { $this->district_created_date = $district_created_date; }
        function set_district_updated_date($district_updated_date) { $this->district_updated_date = $district_updated_date; }

        public function __construct() {
			$db = new DbConnect();
			$this->dbConn = $db->connect();
        }
        
        // get all 
        public function get_all() {
            $stmt = $this->dbConn->prepare("SELECT D.*, R.region_name FROM $this->tableName AS D
                                            LEFT JOIN pos_mgt.regions AS R ON
                                            D.district_region_id = R.region_id
                                            WHERE district_record_hide=:recordHide
                                            ORDER BY district_id DESC"
                                        );
            $stmt->bindParam(':recordHide', $this->district_record_hide);
			$stmt->execute();
			$response = $stmt->fetchAll(PDO::FETCH_ASSOC);
			return $response;
        }
        
        // get  by id
        public function get_by_id(){
            $stmt = $this->dbConn->prepare("SELECT * FROM $this->tableName WHERE district_id=:id AND district_record_hide = :recordHide");
            $stmt->bindParam(':id', $this->district_id);
            $stmt->bindParam(':recordHide', $this->district_record_hide);
			$stmt->execute();
			$response = $stmt->fetch(PDO::FETCH_ASSOC);
			return $response;
        }
        // insert 
        public function insert(){
            $sql = "INSERT INTO $this->tableName(district_region_id, district_name, district_record_hide, district_block_status, district_created_date, district_updated_date)
            VALUES (:regionId, :districtName, :recordHide, :blockStatus, :createdDate, :updateDate)";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(':regionId', $this->district_region_id);
			$stmt->bindParam(':districtName', $this->district_name);
			$stmt->bindParam(':recordHide', $this->district_record_hide);
			$stmt->bindParam(':blockStatus', $this->district_block_status);
			$stmt->bindParam(':createdDate', $this->district_created_date);
			$stmt->bindParam(':updateDate', $this->district_updated_date);
			if($stmt->execute()) {
				return true;
			} else {
				return false;
			}
        }
        // update
        public function update(){
            $sql = "UPDATE $this->tableName SET 
            district_region_id=:regionId,
            district_name=:districtName,
            district_updated_date=:updateDate 
            WHERE district_id=:id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(':regionId', $this->district_region_id);
			$stmt->bindParam(':districtName', $this->district_name);
			$stmt->bindParam(':updateDate', $this->district_updated_date);
			$stmt->bindParam(':id', $this->district_id);
			if($stmt->execute()) {
				return true;
			} else {
				return false;
			}
        }
        // delete
        public function delete(){
            $sql = "UPDATE $this->tableName SET 
            district_record_hide=:recordHide,
            district_updated_date=:updateDate 
            WHERE district_id=:id";
			$stmt = $this->dbConn->prepare($sql);
            $stmt->bindParam(':recordHide', $this->district_record_hide);
            $stmt->bindParam(':updateDate', $this->district_updated_date);
			$stmt->bindParam(':id', $this->district_id);
			if($stmt->execute()) {
				return true;
			} else {
				return false;
			}
        }
        // block status
        public function block_status(){
            $sql = "UPDATE $this->tableName SET 
            district_block_status=:blockStatus,
            district_updated_date=:updateDate 
            WHERE district_id=:id";
			$stmt = $this->dbConn->prepare($sql);
            $stmt->bindParam(':blockStatus', $this->district_block_status);
            $stmt->bindParam(':updateDate', $this->district_updated_date);
			$stmt->bindParam(':id', $this->district_id);
			if($stmt->execute()) {
				return true;
			} else {
				return false;
			}
        }
    }


?>