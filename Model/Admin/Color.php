<?php
    require_once("../Model/db/DbConnect.php");

    class Color {
        private $color_id;
        private $color_name;
        private $color_hex_code;
        private $color_record_hide;
        private $color_block_status;
        private $color_account_type;
        private $color_company_id;
        private $color_branche_id;
        private $color_updated_branche_id;
        private $color_created_date;
        private $color_updated_date;
        private $tableName = 'pos_mgt.colors';
        private $dbConn;

        function set_color_id($color_id) { $this->color_id = $color_id; }
        function set_color_name($color_name) { $this->color_name = $color_name; }
        function set_color_hex_code($color_hex_code) { $this->color_hex_code = $color_hex_code; }
        function set_color_record_hide($color_record_hide) { $this->color_record_hide = $color_record_hide; }
        function set_color_block_status($color_block_status) { $this->color_block_status = $color_block_status; }
        function set_color_account_type($color_account_type) { $this->color_account_type = $color_account_type; }
        function set_color_company_id($color_company_id) { $this->color_company_id = $color_company_id; }
        function set_color_branche_id($color_branche_id) { $this->color_branche_id = $color_branche_id; }
        function set_color_updated_branche_id($color_updated_branche_id) { $this->color_updated_branche_id = $color_updated_branche_id; }
        function set_color_created_date($color_created_date) { $this->color_created_date = $color_created_date; }
        function set_color_updated_date($color_updated_date) { $this->color_updated_date = $color_updated_date; }


        public function __construct() {
			$db = new DbConnect();
			$this->dbConn = $db->connect();
        }
        
        // get all 
        public function get_all() {
            $stmt = $this->dbConn->prepare("SELECT * FROM $this->tableName 
                                            WHERE color_record_hide=:recordHide
                                            AND color_company_id=:companyId
                                            ORDER BY color_id DESC"
                                        );
            $stmt->bindParam(':companyId', $this->color_company_id);
            $stmt->bindParam(':recordHide', $this->color_record_hide);
			$stmt->execute();
			$response = $stmt->fetchAll(PDO::FETCH_ASSOC);
			return $response;
        }
        
        // get  by id
        public function get_by_id(){
            $stmt = $this->dbConn->prepare("SELECT * FROM $this->tableName WHERE color_id=:id AND color_record_hide = :recordHide");
            $stmt->bindParam(':id', $this->color_id);
            $stmt->bindParam(':recordHide', $this->color_record_hide);
			$stmt->execute();
			$response = $stmt->fetch(PDO::FETCH_ASSOC);
			return $response;
        }
        // insert 
        public function insert(){
            $sql = "INSERT INTO $this->tableName( color_name, color_hex_code , color_record_hide, color_block_status, color_account_type, color_company_id, color_branche_id, color_updated_branche_id, color_created_date, color_updated_date)
            VALUES ( :colorName, :coloHexCode , :recordHide, :blockStatus, :accountType, :companyId, :brancheId ,:UpdateBrancheId , :createdDate, :updateDate)";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(':colorName', $this->color_name);
			$stmt->bindParam(':coloHexCode', $this->color_hex_code);
			$stmt->bindParam(':recordHide', $this->color_record_hide);
			$stmt->bindParam(':blockStatus', $this->color_block_status);
			$stmt->bindParam(':accountType', $this->color_account_type);
			$stmt->bindParam(':companyId', $this->color_company_id);
			$stmt->bindParam(':brancheId', $this->color_branche_id);
			$stmt->bindParam(':UpdateBrancheId', $this->color_updated_branche_id);
			$stmt->bindParam(':createdDate', $this->color_created_date);
			$stmt->bindParam(':updateDate', $this->color_updated_date);
			if($stmt->execute()) {
				return true;
			} else {
				return false;
			}

        }
        // update
        public function update(){
            $sql = "UPDATE $this->tableName SET 
            color_name=:colorName,
            color_hex_code=:coloHexCode,
            color_updated_branche_id=:UpdateBrancheId,
            color_updated_date=:updateDate
            WHERE color_id=:id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(':colorName', $this->color_name);
            $stmt->bindParam(':coloHexCode', $this->color_hex_code);
            $stmt->bindParam(':UpdateBrancheId', $this->color_updated_branche_id);
			$stmt->bindParam(':updateDate', $this->color_updated_date);
			$stmt->bindParam(':id', $this->color_id);
			if($stmt->execute()) {
				return true;
			} else {
				return false;
			}
        }
        // delete
        public function delete(){
            $sql = "UPDATE $this->tableName SET 
            color_record_hide=:recordHide,
            color_branche_id=:brancheId,
            color_updated_date=:updateDate 
            WHERE color_id=:id";
			$stmt = $this->dbConn->prepare($sql);
            $stmt->bindParam(':recordHide', $this->color_record_hide);
            $stmt->bindParam(':brancheId', $this->color_updated_branche_id);
            $stmt->bindParam(':updateDate', $this->color_updated_date);
			$stmt->bindParam(':id', $this->color_id);
			if($stmt->execute()) {
				return true;
			} else {
				return false;
			}
        }
        // block status
        public function block_status(){
            $sql = "UPDATE $this->tableName SET 
            color_block_status=:blockStatus,
            color_branche_id=:brancheId,
            color_updated_date=:updateDate 
            WHERE color_id=:id";
			$stmt = $this->dbConn->prepare($sql);
            $stmt->bindParam(':blockStatus', $this->color_block_status);
            $stmt->bindParam(':brancheId', $this->color_updated_branche_id);
            $stmt->bindParam(':updateDate', $this->color_updated_date);
			$stmt->bindParam(':id', $this->color_id);
			if($stmt->execute()) {
				return true;
			} else {
				return false;
			}
        }
    }


?>