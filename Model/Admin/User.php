<?php
    require_once("../Model/db/DbConnect.php");

    class User {
        private $user_id;
        private $user_company_id;
        private $user_branche_id;
        private $user_email;
        private $user_password;
        private $user_password_status;
        private $user_group_id;
        private $user_account_status;
        private $user_record_hide;
        private $user_block_status;
        private $user_created_company_id;
        private $user_created_date;
        private $user_updated_company_id;
        private $user_updated_date;
        private $user_account_type;
        private $user_notes;
        private $tableName = 'pos_mgt.users';
        private $dbConn;

        function set_user_id($user_id) { $this->user_id = $user_id; }
        function set_user_company_id($user_company_id) { $this->user_company_id = $user_company_id; }
        function set_user_branche_id($user_branche_id) { $this->user_branche_id = $user_branche_id; }
        function set_user_email($user_email) { $this->user_email = $user_email; }
        function set_user_password($user_password) { $this->user_password = $user_password; }
        function set_user_password_status($user_password_status) { $this->user_password_status = $user_password_status; }
        function set_user_group_id($user_group_id) { $this->user_group_id = $user_group_id; }
        function set_user_account_status($user_account_status) { $this->user_account_status = $user_account_status; }
        function set_user_record_hide($user_record_hide) { $this->user_record_hide = $user_record_hide; }
        function set_user_block_status($user_block_status) { $this->user_block_status = $user_block_status; }
        function set_user_created_company_id($user_created_company_id) { $this->user_created_company_id = $user_created_company_id; }
        function set_user_created_date($user_created_date) { $this->user_created_date = $user_created_date; }
        function set_user_updated_company_id($user_updated_company_id) { $this->user_updated_company_id = $user_updated_company_id; }
        function set_user_updated_date($user_updated_date) { $this->user_updated_date = $user_updated_date; }
        function set_user_account_type($user_account_type) { $this->user_account_type = $user_account_type; }
        function set_user_notes($user_notes) { $this->user_notes = $user_notes; }


        public function __construct() {
			$db = new DbConnect();
			$this->dbConn = $db->connect();
        }

        // login user
        public function user_login(){
            $stmt = $this->dbConn->prepare("SELECT * FROM $this->tableName 
                                            WHERE user_email=:userEmail 
                                            AND user_account_status=:accountStatus
                                            AND user_record_hide=:recordHide
                                            AND user_block_status=:blockStatus"
                                        );
            $stmt->bindParam(':userEmail', $this->user_email);
            $stmt->bindParam(':accountStatus', $this->user_account_status);
            $stmt->bindParam(':recordHide', $this->user_record_hide);
            $stmt->bindParam(':blockStatus', $this->user_block_status);
			$stmt->execute();
			$response = $stmt->fetch(PDO::FETCH_ASSOC);
			return $response;
        }
        
        // get all users
        public function get_all_users() {
            $stmt = $this->dbConn->prepare("SELECT * FROM $this->tableName WHERE user_record_hide=:recordHide");
            $stmt->bindParam(':recordHide', $this->user_record_hide);
			$stmt->execute();
			$response = $stmt->fetchAll(PDO::FETCH_ASSOC);
			return $response;
        }
        
        // get user by id
        public function get_user_by_id(){
            $stmt = $this->dbConn->prepare("SELECT * FROM $this->tableName WHERE user_id=:id AND user_record_hide = :recordHide");
            $stmt->bindParam(':id', $this->user_id);
            $stmt->bindParam(':recordHide', $this->user_record_hide);
			$stmt->execute();
			$response = $stmt->fetchAll(PDO::FETCH_ASSOC);
			return $response;
        }
        // insert user
        public function insert(){
            $sql = "INSERT INTO $this->tableName( user_company_id, user_branche_id, user_email, user_password, user_group_id, user_account_status, user_record_hide, user_block_status, user_created_company_id, user_created_date, user_updated_company_id, user_updated_date, user_account_type, user_password_status, user_notes)
            VALUES (:companyId, :brancheId, :userEmail, :userPassword, :userGroupId, :userAccountStatus, :userRecordHide, :userBlockStatus, :userCreatedCompanyId, :userCreatedDate, :userUpdatedCompanyId, :userUpdatedDate, :userAccountType, :userPasswordStatus, :userNotes)";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(':companyId', $this->user_company_id);
            $stmt->bindParam(':brancheId', $this->user_branche_id);
            $stmt->bindParam(':userEmail', $this->user_email);
            $stmt->bindParam(':userPassword', $this->user_password);
            $stmt->bindParam(':userGroupId', $this->user_group_id);
            $stmt->bindParam(':userAccountStatus', $this->user_account_status);
            $stmt->bindParam(':userRecordHide', $this->user_record_hide);
            $stmt->bindParam(':userBlockStatus', $this->user_block_status);
            $stmt->bindParam(':userCreatedCompanyId', $this->user_created_company_id);
            $stmt->bindParam(':userCreatedDate', $this->user_created_date);
            $stmt->bindParam(':userUpdatedCompanyId', $this->user_updated_company_id);
            $stmt->bindParam(':userUpdatedDate', $this->user_updated_date);
            $stmt->bindParam(':userAccountType', $this->user_account_type);
            $stmt->bindParam(':userPasswordStatus', $this->user_password_status);
            $stmt->bindParam(':userNotes', $this->user_notes);
			if($stmt->execute()) {
				return true;
			} else {
				return false;
			}
        }
        // update
        public function update(){
            $sql = "UPDATE $this->tableName SET 
            user_email = :userEmail,
            user_password = :userPassword,
            user_group_id = :userGroupId,
            user_account_status = :userAccountStatus,
            user_updated_company_id = :userUpdatedCompanyId,
            user_updated_date = :userUpdatedDate,
            user_account_type = :userAccountType,
            user_password_status = :userPasswordStatus,
            user_notes = :userNotes
            WHERE user_id = :Id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(':userEmail', $this->user_email);
            $stmt->bindParam(':userPassword', $this->user_password);
            $stmt->bindParam(':userGroupId', $this->user_group_id);
            $stmt->bindParam(':userAccountStatus', $this->user_account_status);
            $stmt->bindParam(':userUpdatedCompanyId', $this->user_updated_company_id);
            $stmt->bindParam(':userUpdatedDate', $this->user_updated_date);
            $stmt->bindParam(':userAccountType', $this->user_account_type);
            $stmt->bindParam(':userPasswordStatus', $this->user_password_status);
            $stmt->bindParam(':userNotes', $this->user_notes);
			$stmt->bindParam(':Id', $this->user_id);
			if($stmt->execute()) {
				return true;
			} else {
				return false;
			}
        }
        // delete
        public function delete(){
            $sql = "UPDATE $this->tableName SET 
            user_record_hide=:recordHide,
            user_updated_date=:updateDate,
            user_updated_company_id=:updateCompanyId
            WHERE user_id=:id";
			$stmt = $this->dbConn->prepare($sql);
            $stmt->bindParam(':recordHide', $this->user_record_hide);
            $stmt->bindParam(':updateDate', $this->user_updated_date);
            $stmt->bindParam(':updateCompanyId', $this->user_updated_company_id);
			$stmt->bindParam(':id', $this->user_id);
			if($stmt->execute()) {
				return true;
			} else {
				return false;
			}
        }
        // block status
        public function block_status(){
            $sql = "UPDATE $this->tableName SET 
            user_block_status=:blockStatus,
            user_updated_date=:updateDate,
            user_updated_company_id=:updateCompanyId
            WHERE user_id=:id";
			$stmt = $this->dbConn->prepare($sql);
            $stmt->bindParam(':blockStatus', $this->user_block_status);
            $stmt->bindParam(':updateDate', $this->user_updated_date);
            $stmt->bindParam(':updateCompanyId', $this->user_updated_company_id);
			$stmt->bindParam(':id', $this->user_id);
			if($stmt->execute()) {
				return true;
			} else {
				return false;
			}
        }
    }


?>