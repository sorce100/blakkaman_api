<?php
    require_once("../Model/db/DbConnect.php");

    class Category {
        private $category_id;
        private $category_name;
        private $category_logo;
        private $category_record_hide;
        private $category_block_status;
        private $category_created_date;
        private $category_updated_date;
        private $tableName = 'pos_mgt.categories';
        private $dbConn;

        function set_category_id($category_id) { $this->category_id = $category_id; }
        function set_category_name($category_name) { $this->category_name = $category_name; }
        function set_category_logo($category_logo) { $this->category_logo = $category_logo; }
        function set_category_record_hide($category_record_hide) { $this->category_record_hide = $category_record_hide; }
        function set_category_block_status($category_block_status) { $this->category_block_status = $category_block_status; }
        function set_category_created_date($category_created_date) { $this->category_created_date = $category_created_date; }
        function set_category_updated_date($category_updated_date) { $this->category_updated_date = $category_updated_date; }

        public function __construct() {
			$db = new DbConnect();
			$this->dbConn = $db->connect();
        }
        
        // get all 
        public function get_all() {
            $stmt = $this->dbConn->prepare("SELECT * FROM $this->tableName 
                                            WHERE category_record_hide=:recordHide
                                            ORDER BY category_id DESC"
                                        );
            $stmt->bindParam(':recordHide', $this->category_record_hide);
			$stmt->execute();
			$response = $stmt->fetchAll(PDO::FETCH_ASSOC);
			return $response;
        }
        
        // get by id
        public function get_by_id(){
            $stmt = $this->dbConn->prepare("SELECT * FROM $this->tableName WHERE category_id=:id AND category_record_hide = :recordHide");
            $stmt->bindParam(':id', $this->category_id);
            $stmt->bindParam(':recordHide', $this->category_record_hide);
			$stmt->execute();
			$response = $stmt->fetch(PDO::FETCH_ASSOC);
			return $response;
        }
        // insert 
        public function insert(){
            $sql = "INSERT INTO $this->tableName(category_name, category_logo, category_record_hide, category_block_status, category_created_date, category_updated_date)
            VALUES (:categoryName, :categoryLogo, :recordHide, :blockStatus, :createdDate, :updateDate)";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(':categoryName', $this->category_name);
			$stmt->bindParam(':categoryLogo', $this->category_logo);
			$stmt->bindParam(':recordHide', $this->category_record_hide);
			$stmt->bindParam(':blockStatus', $this->category_block_status);
			$stmt->bindParam(':createdDate', $this->category_created_date);
			$stmt->bindParam(':updateDate', $this->category_updated_date);
			if($stmt->execute()) {
				return true;
			} else {
				return false;
			}
        }
        // update
        public function update(){
            $sql = "UPDATE $this->tableName SET 
            category_name=:categoryName,
            category_logo=:categoryLogo,
            category_updated_date=:updateDate 
            WHERE category_id=:id";
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(':categoryName', $this->category_name);
			$stmt->bindParam(':categoryLogo', $this->category_logo);
			$stmt->bindParam(':updateDate', $this->category_updated_date);
			$stmt->bindParam(':id', $this->category_id);
			if($stmt->execute()) {
				return true;
			} else {
				return false;
			}
        }
        // delete
        public function delete(){
            $sql = "UPDATE $this->tableName SET 
            category_record_hide=:recordHide,
            category_updated_date=:updateDate 
            WHERE category_id=:id";
			$stmt = $this->dbConn->prepare($sql);
            $stmt->bindParam(':recordHide', $this->category_record_hide);
            $stmt->bindParam(':updateDate', $this->category_updated_date);
			$stmt->bindParam(':id', $this->category_id);
			if($stmt->execute()) {
				return true;
			} else {
				return false;
			}
        }
        // block status
        public function block_status(){
            $sql = "UPDATE $this->tableName SET 
            category_block_status=:blockStatus,
            category_updated_date=:updateDate 
            WHERE category_id=:id";
			$stmt = $this->dbConn->prepare($sql);
            $stmt->bindParam(':blockStatus', $this->category_block_status);
            $stmt->bindParam(':updateDate', $this->category_updated_date);
			$stmt->bindParam(':id', $this->category_id);
			if($stmt->execute()) {
				return true;
			} else {
				return false;
			}
        }
    }


?>