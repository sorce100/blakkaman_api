<?php 
	class DbConnect {
	   	private $dbhost = 'localhost';
	   	private $dbuser = 'postgres';
	   	private $dbpass = 'Athens@123';
	   	private $dbname = 'project_courier';

		public function connect() {
			try {
				$db_connect_str = "pgsql:host=$this->dbhost;dbname=$this->dbname";
				$conn = new PDO($db_connect_str, $this->dbuser,$this->dbpass);
				$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				return $conn;
			} catch (Exception $e) {
				echo "Database Error: " . $e->getMessage();
			}
		}

	}
 ?>