<?php
	require_once('constants.php');
	require_once('jwt.php');
	require_once("../Model/db/DbConnect.php");

    class Rest {
    	protected $request;
    	protected $serviceName;
    	protected $param;
    	protected $dbConn;
    	protected $userId;
        // get the method used
        public function __construct(){
        	// check if method is post
        	if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
        		// get defined variable form constants.php
        		$this->throwError(REQUEST_METHOD_NOT_VALID,'Request Method is not valid.');
        	}else{
	        	$handler = fopen('php://input','r');
	        	// returns input into string format
	        	$this->request = stream_get_contents($handler);
	        	$this->validateRequest($this->request);
	        	// create object of database class
				$db = new DBConnect;
				$this->dbConn= $db->connect();
				// 
				if ('generatetoken' != strtolower($this->serviceName)) {
					$this->validateToken();
				}
	        }
        }	
        // generate user tokens
        public function generateToken(){
			try {
				$email = $this->validateParameters('email',$this->param['email'],STRING);
				$pass = $this->validateParameters('pass',$this->param['pass'],STRING);
				// get the credentials from db
				$stmt = $this->dbConn->prepare("SELECT * from api.users where email=:email AND password = :pass");
				$stmt->bindParam(":email",$email);
				$stmt->bindParam(":pass",$pass);
				$stmt->execute();

				$user = $stmt->fetch(PDO::FETCH_ASSOC);
				// check for response
				if (!is_array($user)) {
					$this->returnResponse(INVALID_USER_PASS,"Email or Password is incorrect");
				}
				// check if user is not active
				switch ($user['active']) {
					case 0:
						$this->returnResponse(USER_NOT_ACTIVE,"User is not activated. Please contact Administator");
					break;
				}
				// creating array payload
				$payload = [
					// issued at = current time token is generated
					'iat'=>time(),
					// who issued it
					'iss'=>'localhost',
					// when should token expire (minutes*seconds from the seconds to expire)
					'exp'=>TOKEN_EXPIRY_TIME,
					// user id
					'userId' =>$user['id'],
				];
				// if everything checks out then we create json web token
				$token = JWT::encode($payload,SECRETE_KEY);

				$data = ['token'=>$token];
				$this->returnResponse(SUCCESS_RESPONSE,$data);
				
			} catch (Exception $e) {
				$this->throwError(JWT_PROCESSING_ERROR,$e->getMessage());
			}	
		}

        // validate parameters
        public function validateRequest(){
        	// validate if input is json / content-type or not
        	if ($_SERVER['CONTENT_TYPE'] !== 'application/json') {
        		$this->throwError(REQUEST_CONTENTTYPE_NOT_VALID,'Request Content type is not valid');
        	}
        	else{
        		// decode json input into array
        		$data = json_decode($this->request,true);
        		// check if the service name is isset
        		if(!isset($data["name"]) || empty($data["name"])){
        			$this->throwError(API_NAME_REQUIRED,'Api Name required');
        		}
        		else{
        			$this->serviceName = $data["name"];
        		}
        		// check if decoded json is array
        		if(!is_array($data["param"]) ){
        			$this->throwError(API_PARAM_REQUIRED,'Api PARAM required');
        		}
        		else{
        			$this->param = $data["param"];
        		}
        	}

        }
        //checks service name
        public function processApi($className,$classObject){
        	// $api = new Api;
			// using reflection method calling any function dynamically ('classname','functionName')
			// try {
				$rMethod =  new reflectionMethod($className,$this->serviceName);	
				if (!method_exists($classObject, $this->serviceName)) {
					$this->throwError(API_DOES_NOT_EXIST,'Api does not exit.');
				}
				else{
					$rMethod->invoke($classObject);
				}
			// } catch (Throwable $th) {
			// 	$this->throwError(API_DOES_NOT_EXIST,'Api does not exit.');
			// }
        }
        // for displaying errors
        public function throwError($code,$message){
        	// making response json only
        	header("content-type:application/json");
        	$errorMsg =  json_encode(['error'=>['status'=>$code,'message'=>$message]]);
        	echo $errorMsg;
        	exit;

        }
        // for response for user database authentication
        public function returnResponse($statusCode,$data){
        	header("content-type:application/json");
        	$response =  json_encode(['response'=>['status'=>$statusCode,"message"=>$data]]);
        	echo $response;
        	exit();
        }
        // validate all the parameters whether string or integer
        public function validateParameters($fieldName,$value,$dataType,$required=true){
        	// validate if field is required and not empty
        	if ($required == true && empty($value)) {
        		$this->throwError(VALIDATE_PARAMETER_REQUIRED,$fieldName.' parameter is required');
        	}
        	// check of value is of data type specified
        	switch ($dataType) {
        		case BOOLEAN:
        			if (!is_bool($value)) {
        				$this->throwError(VALIDATE_PARAMETER_DATATYPE,'Data type is not valid for '.$fieldName.' It should be boolean');
        			}
        		break;
        		case INTEGER:
        			if (!is_numeric($value)) {
        				$this->throwError(VALIDATE_PARAMETER_DATATYPE,'Data type is not valid for '.$fieldName.' It should be numeric');
        			}
        		break;
        		case STRING:
        			if (!is_string($value)) {
        				$this->throwError(VALIDATE_PARAMETER_DATATYPE,'Data type is not valid for '.$fieldName.' It should be string');
        			}
        		break;
        		default:
        			$this->throwError(VALIDATE_PARAMETER_DATATYPE,'Data type is not valid for '.$fieldName);
        		break;
        	}
        	return $value;
        }
        /**
	    * Get hearder Authorization
	    * */
	    public function getAuthorizationHeader(){
	        $headers = null;
	        if (isset($_SERVER['Authorization'])) {
	            $headers = trim($_SERVER["Authorization"]);
	        }
	        else if (isset($_SERVER['HTTP_AUTHORIZATION'])) { //Nginx or fast CGI
	            $headers = trim($_SERVER["HTTP_AUTHORIZATION"]);
	        } elseif (function_exists('apache_request_headers')) {
	            $requestHeaders = apache_request_headers();
	            // Server-side fix for bug in old Android versions (a nice side-effect of this fix means we don't care about capitalization for Authorization)
	            $requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));
	            if (isset($requestHeaders['Authorization'])) {
	                $headers = trim($requestHeaders['Authorization']);
	            }
	        }
	        return $headers;
	    }
	    /**
	     * get access token from header
	     * */
	    public function getBearerToken() {
	        $headers = $this->getAuthorizationHeader();
	        // HEADER: Get the access token from the header
	        if (!empty($headers)) {
	            if (preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
	                return $matches[1];
	            }
	        }
	        $this->throwError( ATHORIZATION_HEADER_NOT_FOUND, 'Access Token Not found');
	    }

	    // validating token
	    public function validateToken(){
	    	// get the token from authorization
				$token = $this->getBearerToken();
				// decode joken from authorization into payload array
				$payload = JWT::decode($token,SECRETE_KEY,['HS256']);
				// check if userid is in the db
				$stmt = $this->dbConn->prepare("SELECT * from api.users where id=:userId");
				$stmt->bindParam(":userId",$payload->userId);
				$stmt->execute();
				$user = $stmt->fetch(PDO::FETCH_ASSOC);
				if (!is_array($user)) {
					$this->returnResponse(INVALID_USER_PASS,"This user is not found in our database.");
				}
				// check if user is not active
				switch ($user['active']) {
					case 0:
						$this->returnResponse(USER_NOT_ACTIVE,"This user may be deactivated. Please contact Admin");
					break;
				}

				$this->userId = $payload->userId;
	    }
    }
?>