<?php
    require_once('../Core/rest.php');
    require_once('../Model/Admin/District.php');
    
    class DistrictController extends Rest{
        function __construct(){
			parent::__construct();
        }
        // get all District
        public function getAll(){
            try {
                $districtObj = new District;
                $districtObj->set_district_record_hide("NO");
                $response = $districtObj->get_all();
                if(empty($response)){
					$response = 'No records found';
                }
				$this->returnResponse(SUCCESS_RESPONSE,$response);

            } catch (Exception $e) {
				$this->throwError(ACCESS_TOKEN_ERRORS,$e->getMessage());
			}
        }

        // get region by id
        public function getById(){
            $districtId = $this->validateParameters('district_id', $this->param['district_id'], STRING);
			try {
				// if everything checks out from user authentication then  create object of request and save
				$districtObj = new District;
				$districtObj->set_district_id($districtId);
				$districtObj->set_district_record_hide("NO");
				$response = $districtObj->get_by_id();
				if (!is_array($response) || empty($response)) {
					$this->returnResponse(SUCCESS_RESPONSE,['message' => 'Data is not in database.']);
				}
				else{
					$this->returnResponse(SUCCESS_RESPONSE,$response);
				}

			} catch (Exception $e) {
				$this->throwError(ACCESS_TOKEN_ERRORS,$e->getMessage());
			}
        }
        // insert
        public function addRecord(){
			$regionId = $this->validateParameters('district_region_id', $this->param['district_region_id'], STRING);
			$districtName = $this->validateParameters('district_name', $this->param['district_name'], STRING);
			$date = date('Y-m-d h:m:i');
			try {
				// if everything checks out from user authentication then  create object of request and save
				$districtObj = new District;
				$districtObj->set_district_region_id($regionId);
				$districtObj->set_district_name($districtName);
				$districtObj->set_district_record_hide("NO");
				$districtObj->set_district_block_status("UNBLOCK");
				$districtObj->set_district_created_date($date);
				$districtObj->set_district_updated_date($date);

				if(!$districtObj->insert()){
					$message = 'Failed to insert.';
				}else{
					$message = "Inserted Successfully."; 
				}

				$this->returnResponse(SUCCESS_RESPONSE,$message);
				// $user = $stmt->fetch(PDO::FETCH_ASSOC);
				// print_r($payload->userId);
			} catch (Exception $e) {
				$this->throwError(ACCESS_TOKEN_ERRORS,$e->getMessage());
			}
		}
        // update
        public function updateRecord(){
			$districtId = $this->validateParameters('district_id', $this->param['district_id'], STRING);
			$regionId = $this->validateParameters('district_region_id', $this->param['district_region_id'], STRING);
			$districtName = $this->validateParameters('district_name', $this->param['district_name'], STRING);
			$date = date('Y-m-d h:m:i');
			try {
				// if everything checks out from user authentication then  create object of request and save
				$districtObj = new District;
				$districtObj->set_district_id($districtId);
				$districtObj->set_district_region_id($regionId);
				$districtObj->set_district_name($districtName);
				$districtObj->set_district_updated_date($date);
				if(!$districtObj->update()){
					$message = 'Failed to update.';
				}else{
					$message = "Updated Successfully."; 
				}
				$this->returnResponse(SUCCESS_RESPONSE,$message);
				// $user = $stmt->fetch(PDO::FETCH_ASSOC);
				// print_r($payload->userId);
			} catch (Exception $e) {
				$this->throwError(ACCESS_TOKEN_ERRORS,$e->getMessage());
			}
		}
        // delete
        public function deleteRecord(){
            $districtId = $this->validateParameters('district_id', $this->param['district_id'], STRING);
            $date = date('Y-m-d h:m:i');
			try {
				// if everything checks out from user authentication then  create object of request and save
				$districtObj = new District;
                $districtObj->set_district_id($districtId);
                $districtObj->set_district_updated_date($date);
                $districtObj->set_district_record_hide("YES");
				if (!$districtObj->delete()) {
					$message = 'Failed to delete.';
				}
				// print_r($customer);exit;
				else{
					$message = "Deleted Successfully."; 
				}
				$this->returnResponse(SUCCESS_RESPONSE,$message);
			} catch (Exception $e) {
				$this->throwError(ACCESS_TOKEN_ERRORS,$e->getMessage());
			}
        }
        // update block status
        public function blockStatus(){
			$districtId = $this->validateParameters('district_id', $this->param['district_id'], STRING);
			$districtBlockStatus = $this->validateParameters('district_block_status', $this->param['district_block_status'], STRING);
            $date = date('Y-m-d h:m:i');
			try {
				// if everything checks out from user authentication then  create object of request and save
				$districtObj = new District;
				$districtObj->set_district_id($districtId);
				$districtObj->set_district_block_status($districtBlockStatus);
                $districtObj->set_district_updated_date($date);
				if(!$districtObj->block_status()){
					$message = 'Failed to update.';
				}else{
					$message = "Updated Successfully."; 
				}
				$this->returnResponse(SUCCESS_RESPONSE,$message);
				// $user = $stmt->fetch(PDO::FETCH_ASSOC);
				// print_r($payload->userId);
			} catch (Exception $e) {
				$this->throwError(ACCESS_TOKEN_ERRORS,$e->getMessage());
			}
		}
    }

?>