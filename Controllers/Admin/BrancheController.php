<?php
    require_once('../Core/rest.php');
    require_once('../Model/Admin/Branche.php');
    
    class BrancheController extends Rest{
        function __construct(){
			parent::__construct();
        }
        // get all 
        public function getAll(){
            try {
                $brancheObj = new Branche;
                $brancheObj->set_branche_record_hide("NO");
                $response = $brancheObj->get_all();
                if(empty($response)){
					$response = 'No records found';
                }
				$this->returnResponse(SUCCESS_RESPONSE,$response);

            } catch (Exception $e) {
				$this->throwError(ACCESS_TOKEN_ERRORS,$e->getMessage());
			}
        }

        // get by id
        public function getById(){
            $BrancheId = $this->validateParameters('branche_id', $this->param['branche_id'], STRING);
			try {
				// if everything checks out from user authentication then  create object of request and save
				$brancheObj = new Branche;
				$brancheObj->set_branche_id($BrancheId);
				$brancheObj->set_branche_record_hide("NO");
				$response = $brancheObj->get_by_id();
				if (!is_array($response) || empty($response)) {
					$this->returnResponse(SUCCESS_RESPONSE,['message' => 'Data is not in database.']);
				}
				else{
					$this->returnResponse(SUCCESS_RESPONSE,$response);
				}

			} catch (Exception $e) {
				$this->throwError(ACCESS_TOKEN_ERRORS,$e->getMessage());
			}
        }
        // insert
        public function addRecord(){
			$companyId = $this->validateParameters('branche_company_id ', $this->param['branche_company_id'], STRING);
			$brancheName = $this->validateParameters('branche_name ', $this->param['branche_name'], STRING);
			$brancheLocation = $this->validateParameters('branche_location ', $this->param['branche_location'], STRING);
			$brancheAddress = $this->validateParameters('branche_address ', $this->param['branche_address'], STRING);
			$brancheFname = $this->validateParameters('branche_contact_fname ', $this->param['branche_contact_fname'], STRING);
			$brancheLname = $this->validateParameters('branche_contact_lname ', $this->param['branche_contact_lname'], STRING);
			$brancheTel = $this->validateParameters('branche_tel ', $this->param['branche_tel'], STRING);
			$brancheGhPost = $this->validateParameters('branche_ghana_post ', $this->param['branche_ghana_post'], STRING);
			$brancheLong = $this->validateParameters('branche_longitude ', $this->param['branche_longitude'], STRING);
			$brancheLat = $this->validateParameters('branche_latitude ', $this->param['branche_latitude'], STRING);
			$brancheRegId = $this->validateParameters('branche_region_id ', $this->param['branche_region_id'], STRING);
			$brancheNotes = $this->validateParameters('branche_notes ', $this->param['branche_notes'], STRING);
			$date = date('Y-m-d h:m:i');
			try {
				// if everything checks out from user authentication then  create object of request and save
				$brancheObj = new Branche;
				$brancheObj->set_branche_company_id($companyId);
				$brancheObj->set_branche_name($brancheName);
				$brancheObj->set_branche_location($brancheLocation);
				$brancheObj->set_branche_address($brancheAddress);
				$brancheObj->set_branche_contact_fname($brancheFname);
				$brancheObj->set_branche_contact_lname($brancheLname);
				$brancheObj->set_branche_tel($brancheTel);
				$brancheObj->set_branche_ghana_post($brancheGhPost);
				$brancheObj->set_branche_longitude($brancheLong);
				$brancheObj->set_branche_latitude($brancheLat);
				$brancheObj->set_branche_region_id($brancheRegId);
				$brancheObj->set_branche_notes($brancheNotes);
				$brancheObj->set_branche_record_hide("NO");
				$brancheObj->set_branche_block_status("UNBLOCK");
				$brancheObj->set_branche_created_date($date);
				$brancheObj->set_branche_updated_date($date);

				if(!$brancheObj->insert()){
					$message = 'Failed to insert.';
				}else{
					$message = "Inserted Successfully."; 
				}

				$this->returnResponse(SUCCESS_RESPONSE,$message);
				// $user = $stmt->fetch(PDO::FETCH_ASSOC);
				// print_r($payload->userId);
			} catch (Exception $e) {
				$this->throwError(ACCESS_TOKEN_ERRORS,$e->getMessage());
			}
		}
        // update
        public function updateRecord(){
			$brancheId = $this->validateParameters('branche_id', $this->param['branche_id'], STRING);
			$companyId = $this->validateParameters('branche_company_id ', $this->param['branche_company_id'], STRING);
			$brancheName = $this->validateParameters('branche_name ', $this->param['branche_name'], STRING);
			$brancheLocation = $this->validateParameters('branche_location ', $this->param['branche_location'], STRING);
			$brancheAddress = $this->validateParameters('branche_address ', $this->param['branche_address'], STRING);
			$brancheFname = $this->validateParameters('branche_contact_fname ', $this->param['branche_contact_fname'], STRING);
			$brancheLname = $this->validateParameters('branche_contact_lname ', $this->param['branche_contact_lname'], STRING);
			$brancheTel = $this->validateParameters('branche_tel ', $this->param['branche_tel'], STRING);
			$brancheGhPost = $this->validateParameters('branche_ghana_post ', $this->param['branche_ghana_post'], STRING);
			$brancheLong = $this->validateParameters('branche_longitude ', $this->param['branche_longitude'], STRING);
			$brancheLat = $this->validateParameters('branche_latitude ', $this->param['branche_latitude'], STRING);
			$brancheRegId = $this->validateParameters('branche_region_id ', $this->param['branche_region_id'], STRING);
			$brancheNotes = $this->validateParameters('branche_notes ', $this->param['branche_notes'], STRING);
			$date = date('Y-m-d h:m:i');
			try {
				// if everything checks out from user authentication then  create object of request and save
				$brancheObj = new Branche;
				$brancheObj->set_branche_id($brancheId);
				$brancheObj->set_branche_company_id($companyId);
				$brancheObj->set_branche_name($brancheName);
				$brancheObj->set_branche_location($brancheLocation);
				$brancheObj->set_branche_address($brancheAddress);
				$brancheObj->set_branche_contact_fname($brancheFname);
				$brancheObj->set_branche_contact_lname($brancheLname);
				$brancheObj->set_branche_tel($brancheTel);
				$brancheObj->set_branche_ghana_post($brancheGhPost);
				$brancheObj->set_branche_longitude($brancheLong);
				$brancheObj->set_branche_latitude($brancheLat);
				$brancheObj->set_branche_region_id($brancheRegId);
				$brancheObj->set_branche_notes($brancheNotes);
				$brancheObj->set_branche_updated_date($date);
				if(!$brancheObj->update()){
					$message = 'Failed to update.';
				}else{
					$message = "Updated Successfully."; 
				}
				$this->returnResponse(SUCCESS_RESPONSE,$message);
				// $user = $stmt->fetch(PDO::FETCH_ASSOC);
				// print_r($payload->userId);
			} catch (Exception $e) {
				$this->throwError(ACCESS_TOKEN_ERRORS,$e->getMessage());
			}
		}
        // delete
        public function deleteRecord(){
            $BrancheId = $this->validateParameters('branche_id', $this->param['branche_id'], STRING);
            $date = date('Y-m-d h:m:i');
			try {
				// if everything checks out from user authentication then  create object of request and save
				 $brancheObj = new Branche;
                $brancheObj->set_branche_id($BrancheId);
                $brancheObj->set_branche_updated_date($date);
                $brancheObj->set_branche_record_hide("YES");
				if (!$brancheObj->delete()) {
					$message = 'Failed to delete.';
				}
				// print_r($customer);exit;
				else{
					$message = "Deleted Successfully."; 
				}
				$this->returnResponse(SUCCESS_RESPONSE,$message);
			} catch (Exception $e) {
				$this->throwError(ACCESS_TOKEN_ERRORS,$e->getMessage());
			}
        }
        // update block status
        public function blockStatus(){
			$BrancheId = $this->validateParameters('branche_id', $this->param['branche_id'], STRING);
			$brancheBlockStatus = $this->validateParameters('branche_block_status', $this->param['branche_block_status'], STRING);
            $date = date('Y-m-d h:m:i');
			try {
				// if everything checks out from user authentication then  create object of request and save
				 $brancheObj = new Branche;
				$brancheObj->set_branche_id($BrancheId);
				$brancheObj->set_branche_block_status($brancheBlockStatus);
                $brancheObj->set_branche_updated_date($date);
				if(!$brancheObj->block_status()){
					$message = 'Failed to update.';
				}else{
					$message = "Updated Successfully."; 
				}
				$this->returnResponse(SUCCESS_RESPONSE,$message);
				// $user = $stmt->fetch(PDO::FETCH_ASSOC);
				// print_r($payload->userId);
			} catch (Exception $e) {
				$this->throwError(ACCESS_TOKEN_ERRORS,$e->getMessage());
			}
		}
		// get all sub cateories by category id

    }

?>