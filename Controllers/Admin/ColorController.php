<?php
    require_once('../Core/rest.php');
    require_once('../Model/Admin/Color.php');
    
    class ColorController extends Rest{
        function __construct(){
			parent::__construct();
        }
        // get all 
        public function getAll(){
			$companyId = $this->validateParameters('color_company_id', $this->param['color_company_id'], INTEGER);
            try {
                $colorObj = new Color;
				$colorObj->set_color_record_hide("NO");
				$colorObj->set_color_company_id($companyId);
                $response = $colorObj->get_all();
                if(empty($response)){
					$response = 'No records found';
                }
                
				$this->returnResponse(SUCCESS_RESPONSE,$response);

            } catch (Exception $e) {
				$this->throwError(ACCESS_TOKEN_ERRORS,$e->getMessage());
			}
        }

        // get by id
        public function getById(){
            $colorId = $this->validateParameters('color_id', $this->param['color_id'], INTEGER);
			try {
				// if everything checks out from user authentication then  create object of request and save
				$colorObj = new Color;
				$colorObj->set_color_id($colorId);
				$colorObj->set_color_record_hide("NO");
				$response = $colorObj->get_by_id();
				if (!is_array($response) || empty($response)) {
					$this->returnResponse(SUCCESS_RESPONSE,['message' => 'Data is not in database.']);
				}
				else{
					$this->returnResponse(SUCCESS_RESPONSE,$response);
				}

			} catch (Exception $e) {
				$this->throwError(ACCESS_TOKEN_ERRORS,$e->getMessage());
			}
        }
        // insert
        public function addRecord(){
			$colorName = $this->validateParameters('color_name', $this->param['color_name'], STRING);
			$colorHexCode = $this->validateParameters('color_hex_code', $this->param['color_hex_code'], STRING);
			$accountType = $this->validateParameters('color_account_type', $this->param['color_account_type'], STRING);
			$companyId = $this->validateParameters('color_company_id', $this->param['color_company_id'], INTEGER);
			$brancheId = $this->validateParameters('color_branche_id', $this->param['color_branche_id'], INTEGER);
			$date = date('Y-m-d h:m:i');
			try {
				// if everything checks out from user authentication then  create object of request and save
				$colorObj = new Color;
				$colorObj->set_color_name($colorName);
				$colorObj->set_color_hex_code($colorHexCode);
				$colorObj->set_color_record_hide("NO");
				$colorObj->set_color_block_status("UNBLOCK");
				$colorObj->set_color_account_type($accountType);
				$colorObj->set_color_company_id($companyId);
				$colorObj->set_color_branche_id($brancheId);
				$colorObj->set_color_updated_branche_id($brancheId);
				$colorObj->set_color_created_date($date);
				$colorObj->set_color_updated_date($date);
				
				if(!$colorObj->insert()){
					$message = 'Failed to insert.';
				}else{
					$message = "Inserted Successfully."; 
				}

				$this->returnResponse(SUCCESS_RESPONSE,$message);
				// $user = $stmt->fetch(PDO::FETCH_ASSOC);
				// print_r($payload->userId);
			} catch (Exception $e) {
				$this->throwError(ACCESS_TOKEN_ERRORS,$e->getMessage());
			}
		}
        // update
        public function updateRecord(){
			$colorId = $this->validateParameters('color_id', $this->param['color_id'], INTEGER);
			$colorName = $this->validateParameters('color_name', $this->param['color_name'], STRING);
			$colorHexCode = $this->validateParameters('color_hex_code', $this->param['color_hex_code'], STRING);
			$brancheId = $this->validateParameters('color_branche_id', $this->param['color_branche_id'], INTEGER);
			$date = date('Y-m-d h:m:i');
			try {
				// if everything checks out from user authentication then  create object of request and save
				$colorObj = new Color;
				$colorObj->set_color_id($colorId);
				$colorObj->set_color_name($colorName);
				$colorObj->set_color_hex_code($colorHexCode);
				$colorObj->set_color_updated_branche_id($brancheId);
				$colorObj->set_color_updated_date($date);
				if(!$colorObj->update()){
					$message = 'Failed to update.';
				}else{
					$message = "Updated Successfully."; 
				}
				$this->returnResponse(SUCCESS_RESPONSE,$message);
				// $user = $stmt->fetch(PDO::FETCH_ASSOC);
				// print_r($payload->userId);
			} catch (Exception $e) {
				$this->throwError(ACCESS_TOKEN_ERRORS,$e->getMessage());
			}
		}
        // delete
        public function deleteRecord(){
			$colorId = $this->validateParameters('color_id', $this->param['color_id'], INTEGER);
			$brancheId = $this->validateParameters('color_branche_id', $this->param['color_branche_id'], INTEGER);
            $date = date('Y-m-d h:m:i');
			try {
				// if everything checks out from user authentication then  create object of request and save
				$colorObj = new Color;
				$colorObj->set_color_id($colorId);
				$colorObj->set_color_updated_branche_id($brancheId);
                $colorObj->set_color_updated_date($date);
                $colorObj->set_color_record_hide("YES");
				if (!$colorObj->delete()) {
					$message = 'Failed to delete.';
				}
				// print_r($customer);exit;
				else{
					$message = "Deleted Successfully."; 
				}
				$this->returnResponse(SUCCESS_RESPONSE,$message);
			} catch (Exception $e) {
				$this->throwError(ACCESS_TOKEN_ERRORS,$e->getMessage());
			}
        }
        // update block status
        public function blockStatus(){
			$colorId = $this->validateParameters('color_id', $this->param['color_id'], INTEGER);
			$blockStatus = $this->validateParameters('color_block_status', $this->param['color_block_status'], STRING);
			$brancheId = $this->validateParameters('color_branche_id', $this->param['color_branche_id'], INTEGER);
            $date = date('Y-m-d h:m:i');
			try {
				// if everything checks out from user authentication then  create object of request and save
				$colorObj = new Color;
				$colorObj->set_color_id($colorId);
				$colorObj->set_color_updated_branche_id($brancheId);
				$colorObj->set_color_block_status($blockStatus);
                $colorObj->set_color_updated_date($date);
				if(!$colorObj->block_status()){
					$message = 'Failed to update.';
				}else{
					$message = "Updated Successfully."; 
				}
				$this->returnResponse(SUCCESS_RESPONSE,$message);
				// $user = $stmt->fetch(PDO::FETCH_ASSOC);
				// print_r($payload->userId);
			} catch (Exception $e) {
				$this->throwError(ACCESS_TOKEN_ERRORS,$e->getMessage());
			}
		}
    }

?>