<?php
    require_once('../Core/rest.php');
    require_once('../Model/Admin/Company.php');
    
    class CompanyController extends Rest{
        function __construct(){
			parent::__construct();
        }
        // get all company
        public function getAll(){
            try {
                $companyObj = new Company;
                $companyObj->set_company_record_hide("NO");
                $response = $companyObj->get_all();
                if(empty($response)){
					$response = 'No records found';
                }
				$this->returnResponse(SUCCESS_RESPONSE,$response);

            } catch (Exception $e) {
				$this->throwError(ACCESS_TOKEN_ERRORS,$e->getMessage());
			}
        }

        // get company by id
        public function getById(){
            $companyId = $this->validateParameters('company_id', $this->param['company_id'], INTEGER);
			try {
				// if everything checks out from user authentication then  create object of request and save
				$companyObj = new Company;
				$companyObj->set_company_id($companyId);
				$companyObj->set_company_record_hide("NO");
				$response = $companyObj->get_by_id();
				if (!is_array($response) || empty($response)) {
					$this->returnResponse(SUCCESS_RESPONSE,['message' => 'Data is not in database.']);
				}
				else{
					$this->returnResponse(SUCCESS_RESPONSE,$response);
				}

			} catch (Exception $e) {
				$this->throwError(ACCESS_TOKEN_ERRORS,$e->getMessage());
			}
        }
        // insert
        public function addRecord(){
            // $this->returnResponse(SUCCESS_RESPONSE,$this->param);exit();
			$companyCategory = $this->validateParameters('company_category_id', $this->param['company_category_id'], STRING);
            $companyName = $this->validateParameters('company_name', $this->param['company_name'], STRING);
            $companyManagerFname = $this->validateParameters('company_manager_fname', $this->param['company_manager_fname'], STRING);
            $companyManagerLname = $this->validateParameters('company_manager_lname', $this->param['company_manager_lname'], STRING);
            $companyTel = $this->validateParameters('company_tel', $this->param['company_tel'], STRING);
            $companyEmail = $this->validateParameters('company_email', $this->param['company_email'], STRING);
            $companyWebsite = $this->validateParameters('company_website', $this->param['company_website'], STRING);
            $companyMetaKeywords = $this->validateParameters('company_meta_keywords', $this->param['company_meta_keywords'], STRING);
            $companyMetaDescription = $this->validateParameters('company_meta_description', $this->param['company_meta_description'], STRING);
            $companyCommssion = $this->validateParameters('company_commission', $this->param['company_commission'], STRING);
            $companyRegionId = $this->validateParameters('company_region_id', $this->param['company_region_id'], STRING);
            $companyAddress = $this->validateParameters('company_address', $this->param['company_address'], STRING);
            $companyBank = $this->validateParameters('company_bank', $this->param['company_bank'], STRING);
            $companyBankBranche = $this->validateParameters('company_bank_branche', $this->param['company_bank_branche'], STRING);
            $companyBankAccount = $this->validateParameters('company_bank_account', $this->param['company_bank_account'], STRING);
            $companyFolder = $this->validateParameters('company_folder', $this->param['company_folder'], STRING);
            $companyLogo = $this->validateParameters('company_logo', $this->param['company_logo'], STRING);
            $companyNotes = $this->validateParameters('company_notes', $this->param['company_notes'], STRING, false);
			$date = date('d-m-Y h:m:i');
			try {
				// if everything checks out from user authentication then  create object of request and save
				$companyObj = new Company;
				$companyObj->set_company_category_id($companyCategory);
                $companyObj->set_company_name($companyName);
                $companyObj->set_company_manager_fname($companyManagerFname);
                $companyObj->set_company_manager_lname($companyManagerLname);
                $companyObj->set_company_tel($companyTel);
                $companyObj->set_company_email($companyEmail);
                $companyObj->set_company_website($companyWebsite);
                $companyObj->set_company_meta_keywords($companyMetaKeywords);
                $companyObj->set_company_meta_description($companyMetaDescription);
                $companyObj->set_company_commission($companyCommssion);
                $companyObj->set_company_region_id($companyRegionId);
                $companyObj->set_company_address($companyAddress);
                $companyObj->set_company_bank($companyBank);
                $companyObj->set_company_bank_branche($companyBankBranche);
                $companyObj->set_company_bank_account($companyBankAccount);
                $companyObj->set_company_folder($companyFolder);
                $companyObj->set_company_logo($companyLogo);
                $companyObj->set_company_notes($companyNotes);
                $companyObj->set_company_record_hide("NO");
                $companyObj->set_company_block_status("UNBLOCK");
                $companyObj->set_company_created_date($date);
                $companyObj->set_company_updated_date($date);

				if(!$companyObj->insert()){
					$message = 'Failed to insert.';
				}else{
					$message = "Inserted Successfully."; 
				}

				$this->returnResponse(SUCCESS_RESPONSE,$message);
				// $user = $stmt->fetch(PDO::FETCH_ASSOC);
				// print_r($payload->userId);
			} catch (Exception $e) {
				$this->throwError(ACCESS_TOKEN_ERRORS,$e->getMessage());
			}
		}
        // update
        public function updateRecord(){
			$companyId = $this->validateParameters('company_id', $this->param['company_id'], STRING);
			$companyCategory = $this->validateParameters('company_category_id', $this->param['company_category_id'], STRING);
            $companyName = $this->validateParameters('company_name', $this->param['company_name'], STRING);
            $companyManagerFname = $this->validateParameters('company_manager_fname', $this->param['company_manager_fname'], STRING);
            $companyManagerLname = $this->validateParameters('company_manager_lname', $this->param['company_manager_lname'], STRING);
            $companyTel = $this->validateParameters('company_tel', $this->param['company_tel'], STRING);
            $companyEmail = $this->validateParameters('company_email', $this->param['company_email'], STRING);
            $companyWebsite = $this->validateParameters('company_website', $this->param['company_website'], STRING);
            $companyMetaKeywords = $this->validateParameters('company_meta_keywords', $this->param['company_meta_keywords'], STRING);
            $companyMetaDescription = $this->validateParameters('company_meta_description', $this->param['company_meta_description'], STRING);
            $companyCommssion = $this->validateParameters('company_commission', $this->param['company_commission'], STRING);
            $companyRegionId = $this->validateParameters('company_region_id', $this->param['company_region_id'], STRING);
            $companyAddress = $this->validateParameters('company_address', $this->param['company_address'], STRING);
            $companyBank = $this->validateParameters('company_bank', $this->param['company_bank'], STRING);
            $companyBankBranche = $this->validateParameters('company_bank_branche', $this->param['company_bank_branche'], STRING);
            $companyBankAccount = $this->validateParameters('company_bank_account', $this->param['company_bank_account'], STRING);
            $companyLogo = $this->validateParameters('company_logo', $this->param['company_logo'], STRING);
            $companyNotes = $this->validateParameters('company_notes', $this->param['company_notes'], STRING, false);
			$date = date('d-m-Y h:m:i');
			try {
				// if everything checks out from user authentication then  create object of request and save
				$companyObj = new Company;
				$companyObj->set_company_id($companyId);
				$companyObj->set_company_category_id($companyCategory);
                $companyObj->set_company_name($companyName);
                $companyObj->set_company_manager_fname($companyManagerFname);
                $companyObj->set_company_manager_lname($companyManagerLname);
                $companyObj->set_company_tel($companyTel);
                $companyObj->set_company_email($companyEmail);
                $companyObj->set_company_website($companyWebsite);
                $companyObj->set_company_meta_keywords($companyMetaKeywords);
                $companyObj->set_company_meta_description($companyMetaDescription);
                $companyObj->set_company_commission($companyCommssion);
                $companyObj->set_company_region_id($companyRegionId);
                $companyObj->set_company_address($companyAddress);
                $companyObj->set_company_bank($companyBank);
                $companyObj->set_company_bank_branche($companyBankBranche);
                $companyObj->set_company_bank_account($companyBankAccount);
                $companyObj->set_company_logo($companyLogo);
                $companyObj->set_company_notes($companyNotes);
                $companyObj->set_company_updated_date($date);

				if(!$companyObj->update()){
					$message = 'Failed to update.';
				}else{
					$message = "Updated Successfully."; 
				}
				$this->returnResponse(SUCCESS_RESPONSE,$message);
				// $user = $stmt->fetch(PDO::FETCH_ASSOC);
				// print_r($payload->userId);
			} catch (Exception $e) {
				$this->throwError(ACCESS_TOKEN_ERRORS,$e->getMessage());
			}
		}
        // delete
        public function deleteRecord(){
            $companyId = $this->validateParameters('company_id', $this->param['company_id'], INTEGER);
            $date = date('Y-m-d h:m:i');
			try {
				// if everything checks out from user authentication then  create object of request and save
				$companyObj = new Company;
                $companyObj->set_company_id($companyId);
                $companyObj->set_company_updated_date($date);
                $companyObj->set_company_record_hide("YES");
				if (!$companyObj->delete()) {
					$message = 'Failed to delete.';
				}
				// print_r($customer);exit;
				else{
					$message = "Deleted Successfully."; 
				}
				$this->returnResponse(SUCCESS_RESPONSE,$message);
			} catch (Exception $e) {
				$this->throwError(ACCESS_TOKEN_ERRORS,$e->getMessage());
			}
        }
        // update block status
        public function blockStatus(){
			$companyId = $this->validateParameters('company_id', $this->param['company_id'], INTEGER);
			$companyBlockStatus = $this->validateParameters('company_block_status', $this->param['company_block_status'], STRING);
            $date = date('Y-m-d h:m:i');
			try {
				// if everything checks out from user authentication then  create object of request and save
				$companyObj = new Company;
				$companyObj->set_company_id($companyId);
				$companyObj->set_company_block_status($companyBlockStatus);
				$companyObj->set_company_updated_date($date);
				if(!$companyObj->block_status()){
					$message = 'Failed to update.';
				}else{
					$message = "Updated Successfully."; 
				}
				$this->returnResponse(SUCCESS_RESPONSE,$message);
				// $user = $stmt->fetch(PDO::FETCH_ASSOC);
				// print_r($payload->userId);
			} catch (Exception $e) {
				$this->throwError(ACCESS_TOKEN_ERRORS,$e->getMessage());
			}
		}
    }

?>