<?php
    require_once('../Core/rest.php');
    require_once('../Model/Admin/Size.php');
    
    class SizeController extends Rest{
        function __construct(){
			parent::__construct();
        }
        // get all 
        public function getAll(){
            $companyId = $this->validateParameters('size_company_id', $this->param['size_company_id'], INTEGER);
            try {
                $sizeObj = new Size;
                $sizeObj->set_size_company_id($companyId);
                $sizeObj->set_size_record_hide("NO");
                $response = $sizeObj->get_all();
                if(empty($response)){
					$response = 'No records found';
                }
                
				$this->returnResponse(SUCCESS_RESPONSE,$response);

            } catch (Exception $e) {
				$this->throwError(ACCESS_TOKEN_ERRORS,$e->getMessage());
			}
        }

        // get by id
        public function getById(){
            $sizeId = $this->validateParameters('size_id', $this->param['size_id'], INTEGER);
			try {
				// if everything checks out from user authentication then  create object of request and save
				$sizeObj = new Size;
				$sizeObj->set_size_id($sizeId);
				$sizeObj->set_size_record_hide("NO");
				$response = $sizeObj->get_by_id();
				if (!is_array($response) || empty($response)) {
					$this->returnResponse(SUCCESS_RESPONSE,['message' => 'Data is not in database.']);
				}
				else{
					$this->returnResponse(SUCCESS_RESPONSE,$response);
				}

			} catch (Exception $e) {
				$this->throwError(ACCESS_TOKEN_ERRORS,$e->getMessage());
			}
        }
        // insert
        public function addRecord(){
			$sizeName = $this->validateParameters('size_name', $this->param['size_name'], STRING);
			$companyId = $this->validateParameters('size_company_id', $this->param['size_company_id'], INTEGER);
			$brancheId = $this->validateParameters('size_branche_id', $this->param['size_branche_id'], INTEGER);
			$accountType = $this->validateParameters('size_account_type', $this->param['size_account_type'], STRING);
			$updateBrancheId = $this->validateParameters('size_branche_id', $this->param['size_branche_id'], INTEGER);
			$date = date('Y-m-d h:m:i');
			try {
				// if everything checks out from user authentication then  create object of request and save
				$sizeObj = new Size;
				$sizeObj->set_size_name($sizeName);
				$sizeObj->set_size_company_id($companyId);
				$sizeObj->set_size_branche_id($brancheId);
				$sizeObj->set_size_record_hide('NO');
				$sizeObj->set_size_block_status('UNBLOCK');
				$sizeObj->set_size_account_type($accountType);
				$sizeObj->set_size_created_date($date);
				$sizeObj->set_size_update_branche_id($updateBrancheId);
				$sizeObj->set_size_updated_date($date);

				if(!$sizeObj->insert()){
					$message = 'Failed to insert.';
				}else{
					$message = "Inserted Successfully."; 
				}

				$this->returnResponse(SUCCESS_RESPONSE,$message);
				// $user = $stmt->fetch(PDO::FETCH_ASSOC);
				// print_r($payload->userId);
			} catch (Exception $e) {
				$this->throwError(ACCESS_TOKEN_ERRORS,$e->getMessage());
			}
		}
        // update
        public function updateRecord(){
			$sizeId = $this->validateParameters('size_id', $this->param['size_id'], INTEGER);
			$sizeName = $this->validateParameters('size_name', $this->param['size_name'], STRING);
			$updateBrancheId = $this->validateParameters('size_branche_id', $this->param['size_branche_id'], INTEGER);
			$date = date('Y-m-d h:m:i');
			try {
				// if everything checks out from user authentication then  create object of request and save
				$sizeObj = new Size;
				$sizeObj->set_size_id($sizeId);
				$sizeObj->set_size_name($sizeName);
				$sizeObj->set_size_update_branche_id($updateBrancheId);
				$sizeObj->set_size_updated_date($date);

				if(!$sizeObj->update()){
					$message = 'Failed to insert.';
				}else{
					$message = "Updated Successfully."; 
				}
				$this->returnResponse(SUCCESS_RESPONSE,$message);
				// $user = $stmt->fetch(PDO::FETCH_ASSOC);
				// print_r($payload->userId);
			} catch (Exception $e) {
				$this->throwError(ACCESS_TOKEN_ERRORS,$e->getMessage());
			}
		}
        // delete
        public function deleteRecord(){
            $sizeId = $this->validateParameters('size_id', $this->param['size_id'], INTEGER);
            $updateBrancheId = $this->validateParameters('size_branche_id', $this->param['size_branche_id'], INTEGER);
            $date = date('Y-m-d h:m:i');
			try {
				// if everything checks out from user authentication then  create object of request and save
				$sizeObj = new Size;
                $sizeObj->set_size_id($sizeId);
                $sizeObj->set_size_update_branche_id($updateBrancheId);
                $sizeObj->set_size_updated_date($date);
                $sizeObj->set_size_record_hide("YES");
				if (!$sizeObj->delete()) {
					$message = 'Failed to delete.';
				}
				// print_r($customer);exit;
				else{
					$message = "Deleted Successfully."; 
				}
				$this->returnResponse(SUCCESS_RESPONSE,$message);
			} catch (Exception $e) {
				$this->throwError(ACCESS_TOKEN_ERRORS,$e->getMessage());
			}
        }
        // update block status
        public function blockStatus(){
			$sizeId = $this->validateParameters('size_id', $this->param['size_id'], INTEGER);
			$sizeBlockStatus = $this->validateParameters('size_block_status', $this->param['size_block_status'], STRING);
            $updateBrancheId = $this->validateParameters('size_branche_id', $this->param['size_branche_id'], INTEGER);
            $date = date('Y-m-d h:m:i');
			try {
				// if everything checks out from user authentication then  create object of request and save
				$sizeObj = new Size;
				$sizeObj->set_size_id($sizeId);
				$sizeObj->set_size_block_status($sizeBlockStatus);
				$sizeObj->set_size_update_branche_id($updateBrancheId);
				$sizeObj->set_size_updated_date($date);
				if(!$sizeObj->block_status()){
					$message = 'Failed to update.';
				}else{
					$message = "Updated Successfully."; 
				}
				$this->returnResponse(SUCCESS_RESPONSE,$message);
				// $user = $stmt->fetch(PDO::FETCH_ASSOC);
				// print_r($payload->userId);
			} catch (Exception $e) {
				$this->throwError(ACCESS_TOKEN_ERRORS,$e->getMessage());
			}
		}
    }

?>