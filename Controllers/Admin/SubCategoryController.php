<?php
    require_once('../Core/rest.php');
    require_once('../Model/Admin/SubCategory.php');
    
    class SubCategoryController extends Rest{
        function __construct(){
			parent::__construct();
        }
        // get all 
        public function getAll(){
            try {
                $subCategoryObj = new SubCategory;
                $subCategoryObj->set_sub_category_record_hide("NO");
                $response = $subCategoryObj->get_all();
                if(empty($response)){
					$response = 'No records found';
                }
				$this->returnResponse(SUCCESS_RESPONSE,$response);

            } catch (Exception $e) {
				$this->throwError(ACCESS_TOKEN_ERRORS,$e->getMessage());
			}
        }

        // get by id
        public function getById(){
            $subCategoryId = $this->validateParameters('sub_category_id', $this->param['sub_category_id'], STRING);
			try {
				// if everything checks out from user authentication then  create object of request and save
				$subCategoryObj = new SubCategory;
				$subCategoryObj->set_sub_category_id($subCategoryId);
				$subCategoryObj->set_sub_category_record_hide("NO");
				$response = $subCategoryObj->get_by_id();
				if (!is_array($response) || empty($response)) {
					$this->returnResponse(SUCCESS_RESPONSE,['message' => 'Data is not in database.']);
				}
				else{
					$this->returnResponse(SUCCESS_RESPONSE,$response);
				}

			} catch (Exception $e) {
				$this->throwError(ACCESS_TOKEN_ERRORS,$e->getMessage());
			}
        }
        // insert
        public function addRecord(){
			$mainCategoryId = $this->validateParameters('sub_main_category_id', $this->param['sub_main_category_id'], STRING);
			$categoryName = $this->validateParameters('sub_category_name', $this->param['sub_category_name'], STRING);
			$date = date('Y-m-d h:m:i');
			try {
				// if everything checks out from user authentication then  create object of request and save
				$subCategoryObj = new SubCategory;
				$subCategoryObj->set_sub_main_category_id($mainCategoryId);
				$subCategoryObj->set_sub_category_name($categoryName);
				$subCategoryObj->set_sub_category_record_hide("NO");
				$subCategoryObj->set_sub_category_block_status("UNBLOCK");
				$subCategoryObj->set_sub_category_created_date($date);
				$subCategoryObj->set_sub_category_updated_date($date);

				if(!$subCategoryObj->insert()){
					$message = 'Failed to insert.';
				}else{
					$message = "Inserted Successfully."; 
				}

				$this->returnResponse(SUCCESS_RESPONSE,$message);
				// $user = $stmt->fetch(PDO::FETCH_ASSOC);
				// print_r($payload->userId);
			} catch (Exception $e) {
				$this->throwError(ACCESS_TOKEN_ERRORS,$e->getMessage());
			}
		}
        // update
        public function updateRecord(){
			$mainCategoryId = $this->validateParameters('sub_main_category_id', $this->param['sub_main_category_id'], STRING);
			$categoryName = $this->validateParameters('sub_category_name', $this->param['sub_category_name'], STRING);
			$subCategoryId = $this->validateParameters('sub_category_id', $this->param['sub_category_id'], STRING);
			$date = date('Y-m-d h:m:i');
			try {
				// if everything checks out from user authentication then  create object of request and save
				$subCategoryObj = new SubCategory;
				$subCategoryObj->set_sub_category_id($subCategoryId);
				$subCategoryObj->set_sub_main_category_id($mainCategoryId);
				$subCategoryObj->set_sub_category_name($categoryName);
				$subCategoryObj->set_sub_category_updated_date($date);
				if(!$subCategoryObj->update()){
					$message = 'Failed to update.';
				}else{
					$message = "Updated Successfully."; 
				}
				$this->returnResponse(SUCCESS_RESPONSE,$message);
				// $user = $stmt->fetch(PDO::FETCH_ASSOC);
				// print_r($payload->userId);
			} catch (Exception $e) {
				$this->throwError(ACCESS_TOKEN_ERRORS,$e->getMessage());
			}
		}
        // delete
        public function deleteRecord(){
            $subCategoryId = $this->validateParameters('sub_category_id', $this->param['sub_category_id'], STRING);
            $date = date('Y-m-d h:m:i');
			try {
				// if everything checks out from user authentication then  create object of request and save
				$subCategoryObj = new SubCategory;
                $subCategoryObj->set_sub_category_id($subCategoryId);
                $subCategoryObj->set_sub_category_updated_date($date);
                $subCategoryObj->set_sub_category_record_hide("YES");
				if (!$subCategoryObj->delete()) {
					$message = 'Failed to delete.';
				}
				// print_r($customer);exit;
				else{
					$message = "Deleted Successfully."; 
				}
				$this->returnResponse(SUCCESS_RESPONSE,$message);
			} catch (Exception $e) {
				$this->throwError(ACCESS_TOKEN_ERRORS,$e->getMessage());
			}
        }
        // update block status
        public function blockStatus(){
			$subCategoryId = $this->validateParameters('sub_category_id', $this->param['sub_category_id'], STRING);
			$subCategoryBlockStatus = $this->validateParameters('sub_category_block_status', $this->param['sub_category_block_status'], STRING);
            $date = date('Y-m-d h:m:i');
			try {
				// if everything checks out from user authentication then  create object of request and save
				$subCategoryObj = new SubCategory;
				$subCategoryObj->set_sub_category_id($subCategoryId);
				$subCategoryObj->set_sub_category_block_status($subCategoryBlockStatus);
                $subCategoryObj->set_sub_category_updated_date($date);
				if(!$subCategoryObj->block_status()){
					$message = 'Failed to update.';
				}else{
					$message = "Updated Successfully."; 
				}
				$this->returnResponse(SUCCESS_RESPONSE,$message);
				// $user = $stmt->fetch(PDO::FETCH_ASSOC);
				// print_r($payload->userId);
			} catch (Exception $e) {
				$this->throwError(ACCESS_TOKEN_ERRORS,$e->getMessage());
			}
		}
		// get all sub cateories by category id

    }

?>