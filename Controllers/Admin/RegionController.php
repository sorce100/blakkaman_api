<?php
    require_once('../Core/rest.php');
    require_once('../Model/Admin/Region.php');
    
    class RegionController extends Rest{
        function __construct(){
			parent::__construct();
        }
        // get all 
        public function getAll(){
            try {
                $regionObj = new Region;
                $regionObj->set_region_record_hide("NO");
                $response = $regionObj->get_all();
                if(empty($response)){
					$response = 'No records found';
                }
                
				$this->returnResponse(SUCCESS_RESPONSE,$response);

            } catch (Exception $e) {
				$this->throwError(ACCESS_TOKEN_ERRORS,$e->getMessage());
			}
        }

        // get by id
        public function getById(){
            $regionId = $this->validateParameters('region_id', $this->param['region_id'], INTEGER);
			try {
				// if everything checks out from user authentication then  create object of request and save
				$regionObj = new Region;
				$regionObj->set_region_id($regionId);
				$regionObj->set_region_record_hide("NO");
				$response = $regionObj->get_by_id();
				if (!is_array($response) || empty($response)) {
					$this->returnResponse(SUCCESS_RESPONSE,['message' => 'Data is not in database.']);
				}
				else{
					$this->returnResponse(SUCCESS_RESPONSE,$response);
				}

			} catch (Exception $e) {
				$this->throwError(ACCESS_TOKEN_ERRORS,$e->getMessage());
			}
        }
        // insert
        public function addRecord(){
			$regionName = $this->validateParameters('region_name', $this->param['region_name'], STRING);
			$date = date('Y-m-d h:m:i');
			try {
				// if everything checks out from user authentication then  create object of request and save
				$regionObj = new Region;
				$regionObj->set_region_name($regionName);
				$regionObj->set_region_record_hide("NO");
				$regionObj->set_region_block_status("UNBLOCK");
				$regionObj->set_region_created_date($date);
				$regionObj->set_region_updated_date($date);

				if(!$regionObj->insert()){
					$message = 'Failed to insert.';
				}else{
					$message = "Inserted Successfully."; 
				}

				$this->returnResponse(SUCCESS_RESPONSE,$message);
				// $user = $stmt->fetch(PDO::FETCH_ASSOC);
				// print_r($payload->userId);
			} catch (Exception $e) {
				$this->throwError(ACCESS_TOKEN_ERRORS,$e->getMessage());
			}
		}
        // update
        public function updateRecord(){
			$regionId = $this->validateParameters('region_id', $this->param['region_id'], INTEGER);
			$regionName = $this->validateParameters('region_name', $this->param['region_name'], STRING);
			$date = date('Y-m-d h:m:i');
			try {
				// if everything checks out from user authentication then  create object of request and save
				$regionObj = new Region;
				$regionObj->set_region_id($regionId);
				$regionObj->set_region_name($regionName);
				$regionObj->set_region_updated_date($date);

				if(!$regionObj->update()){
					$message = 'Failed to update.';
				}else{
					$message = "Updated Successfully."; 
				}
				$this->returnResponse(SUCCESS_RESPONSE,$message);
				// $user = $stmt->fetch(PDO::FETCH_ASSOC);
				// print_r($payload->userId);
			} catch (Exception $e) {
				$this->throwError(ACCESS_TOKEN_ERRORS,$e->getMessage());
			}
		}
        // delete
        public function deleteRecord(){
            $regionId = $this->validateParameters('region_id', $this->param['region_id'], INTEGER);
            $date = date('Y-m-d h:m:i');
			try {
				// if everything checks out from user authentication then  create object of request and save
				$regionObj = new Region;
                $regionObj->set_region_id($regionId);
                $regionObj->set_region_updated_date($date);
                $regionObj->set_region_record_hide("YES");
				if (!$regionObj->delete()) {
					$message = 'Failed to delete.';
				}
				// print_r($customer);exit;
				else{
					$message = "Deleted Successfully."; 
				}
				$this->returnResponse(SUCCESS_RESPONSE,$message);
			} catch (Exception $e) {
				$this->throwError(ACCESS_TOKEN_ERRORS,$e->getMessage());
			}
        }
        // update block status
        public function blockStatus(){
			$regionId = $this->validateParameters('region_id', $this->param['region_id'], INTEGER);
			$regionBlockStatus = $this->validateParameters('region_block_status', $this->param['region_block_status'], STRING);
            $date = date('Y-m-d h:m:i');
			try {
				// if everything checks out from user authentication then  create object of request and save
				$regionObj = new Region;
				$regionObj->set_region_id($regionId);
				$regionObj->set_region_block_status($regionBlockStatus);
                $regionObj->set_region_updated_date($date);
				if(!$regionObj->block_status()){
					$message = 'Failed to update.';
				}else{
					$message = "Updated Successfully."; 
				}
				$this->returnResponse(SUCCESS_RESPONSE,$message);
				// $user = $stmt->fetch(PDO::FETCH_ASSOC);
				// print_r($payload->userId);
			} catch (Exception $e) {
				$this->throwError(ACCESS_TOKEN_ERRORS,$e->getMessage());
			}
		}
    }

?>