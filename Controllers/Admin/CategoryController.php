<?php
    require_once('../Core/rest.php');
    require_once('../Model/Admin/Category.php');
    
    class CategoryController extends Rest{
        function __construct(){
			parent::__construct();
        }
        // get all 
        public function getAll(){
            try {
                $categoryObj = new Category;
                $categoryObj->set_category_record_hide("NO");
                $response = $categoryObj->get_all();
                if(empty($response)){
					$response = 'No records found';
                }
				$this->returnResponse(SUCCESS_RESPONSE,$response);

            } catch (Exception $e) {
				$this->throwError(ACCESS_TOKEN_ERRORS,$e->getMessage());
			}
        }

        // get by id
        public function getById(){
            $districtId = $this->validateParameters('category_id', $this->param['category_id'], STRING);
			try {
				// if everything checks out from user authentication then  create object of request and save
				$categoryObj = new Category;
				$categoryObj->set_category_id($districtId);
				$categoryObj->set_category_record_hide("NO");
				$response = $categoryObj->get_by_id();
				if (!is_array($response) || empty($response)) {
					$this->returnResponse(SUCCESS_RESPONSE,['message' => 'Data is not in database.']);
				}
				else{
					$this->returnResponse(SUCCESS_RESPONSE,$response);
				}

			} catch (Exception $e) {
				$this->throwError(ACCESS_TOKEN_ERRORS,$e->getMessage());
			}
        }
        // insert
        public function addRecord(){
			$categoryName = $this->validateParameters('category_name', $this->param['category_name'], STRING);
			$categoryLogo = $this->validateParameters('category_logo', $this->param['category_logo'], STRING);
			$date = date('Y-m-d h:m:i');
			try {
				// if everything checks out from user authentication then  create object of request and save
				$categoryObj = new Category;
				$categoryObj->set_category_name($categoryName);
				$categoryObj->set_category_logo($categoryLogo);
				$categoryObj->set_category_record_hide("NO");
				$categoryObj->set_category_block_status("UNBLOCK");
				$categoryObj->set_category_created_date($date);
				$categoryObj->set_category_updated_date($date);

				if(!$categoryObj->insert()){
					$message = 'Failed to insert.';
				}else{
					$message = "Inserted Successfully."; 
				}

				$this->returnResponse(SUCCESS_RESPONSE,$message);
				// $user = $stmt->fetch(PDO::FETCH_ASSOC);
				// print_r($payload->userId);
			} catch (Exception $e) {
				$this->throwError(ACCESS_TOKEN_ERRORS,$e->getMessage());
			}
		}
        // update
        public function updateRecord(){
			$categoryId = $this->validateParameters('category_id', $this->param['category_id'], STRING);
			$categoryName = $this->validateParameters('category_name', $this->param['category_name'], STRING);
			$categoryLogo = $this->validateParameters('category_logo', $this->param['category_logo'], STRING);
			$date = date('Y-m-d h:m:i');
			try {
				// if everything checks out from user authentication then  create object of request and save
				$categoryObj = new Category;
				$categoryObj->set_category_id($categoryId);
				$categoryObj->set_category_name($categoryName);
				$categoryObj->set_category_logo($categoryLogo);
				$categoryObj->set_category_updated_date($date);
				if(!$categoryObj->update()){
					$message = 'Failed to update.';
				}else{
					$message = "Updated Successfully."; 
				}
				$this->returnResponse(SUCCESS_RESPONSE,$message);
				// $user = $stmt->fetch(PDO::FETCH_ASSOC);
				// print_r($payload->userId);
			} catch (Exception $e) {
				$this->throwError(ACCESS_TOKEN_ERRORS,$e->getMessage());
			}
		}
        // delete
        public function deleteRecord(){
            $categoryId = $this->validateParameters('category_id', $this->param['category_id'], STRING);
            $date = date('Y-m-d h:m:i');
			try {
				// if everything checks out from user authentication then  create object of request and save
				$categoryObj = new Category;
                $categoryObj->set_category_id($categoryId);
                $categoryObj->set_category_updated_date($date);
                $categoryObj->set_category_record_hide("YES");
				if (!$categoryObj->delete()) {
					$message = 'Failed to delete.';
				}
				// print_r($customer);exit;
				else{
					$message = "Deleted Successfully."; 
				}
				$this->returnResponse(SUCCESS_RESPONSE,$message);
			} catch (Exception $e) {
				$this->throwError(ACCESS_TOKEN_ERRORS,$e->getMessage());
			}
        }
        // update block status
        public function blockStatus(){
			$districtId = $this->validateParameters('category_id', $this->param['category_id'], STRING);
			$blockStatus = $this->validateParameters('category_block_status', $this->param['category_block_status'], STRING);
            $date = date('Y-m-d h:m:i');
			try {
				// if everything checks out from user authentication then  create object of request and save
				$categoryObj = new Category;
				$categoryObj->set_category_id($districtId);
				$categoryObj->set_category_block_status($blockStatus);
                $categoryObj->set_category_updated_date($date);
				if(!$categoryObj->block_status()){
					$message = 'Failed to update.';
				}else{
					$message = "Updated Successfully."; 
				}
				$this->returnResponse(SUCCESS_RESPONSE,$message);
				// $user = $stmt->fetch(PDO::FETCH_ASSOC);
				// print_r($payload->userId);
			} catch (Exception $e) {
				$this->throwError(ACCESS_TOKEN_ERRORS,$e->getMessage());
			}
		}
    }

?>