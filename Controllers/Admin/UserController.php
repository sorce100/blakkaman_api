<?php
    require_once('../Core/rest.php');
    require_once('../Model/Admin/User.php');
    
    class UserController extends Rest{
        function __construct(){
			parent::__construct();
        }
        // login
        public function userLogin(){
            $userEmail = $this->validateParameters('user_email', $this->param['user_email'], STRING);
            try {
                $userObj = new User;
                $userObj->set_user_email($userEmail);
                $userObj->set_user_block_status("UNBLOCK");
                $userObj->set_user_account_status("ACTIVE");
                $userObj->set_user_record_hide("NO");
                $response = $userObj->user_login();
                if(empty($response)){
					$response = 'User not found, Contact Administrator';
                }
                
                $this->returnResponse(SUCCESS_RESPONSE,$response);
                

            } catch (Exception $e) {
				$this->throwError(ACCESS_TOKEN_ERRORS,$e->getMessage());
			}
        }

        // get all users
        public function getAll(){
            try {
                $userObj = new User;
                $userObj->set_user_record_hide("NO");
                $response = $userObj->get_all();
                if(!is_array($response) || empty($response)){
					$response = 'No records found';
                }
                
				$this->returnResponse(SUCCESS_RESPONSE,$response);

            } catch (Exception $e) {
				$this->throwError(ACCESS_TOKEN_ERRORS,$e->getMessage());
			}
        }

        // get user by id
        public function getById(){
            $userId = $this->validateParameters('user_id', $this->param['user_id'], INTEGER);
			try {
				// if everything checks out from user authentication then  create object of request and save
				$userObj = new User;
				$userObj->set_user_id($userId);
				$userObj->set_user_record_hide("NO");
				$response = $userObj->get_by_id();
				if (!is_array($response) || empty($response)) {
					$this->returnResponse(SUCCESS_RESPONSE,['message' => 'User is not in database.']);
				}
				else{
					$this->returnResponse(SUCCESS_RESPONSE,$response);
				}

			} catch (Exception $e) {
				$this->throwError(ACCESS_TOKEN_ERRORS,$e->getMessage());
			}
        }
        // insert
        public function addRecord(){
			$companyId = $this->validateParameters('user_company_id', $this->param['user_company_id'], INTEGER);
			$brancheId = $this->validateParameters('user_branche_id', $this->param['user_branche_id'], INTEGER);
			$email = $this->validateParameters('user_email', $this->param['user_email'], STRING);
			$password = $this->validateParameters('user_password', $this->param['user_password'], STRING);
			$passwordStatus = $this->validateParameters('user_password_status', $this->param['user_password_status'], STRING);
			$groupId = $this->validateParameters('user_group_id', $this->param['user_group_id'], INTEGER);
			$accountStatus = $this->validateParameters('user_account_status', $this->param['user_account_status'], STRING);
			$createdCompanyId = $this->validateParameters('user_created_company_id', $this->param['user_created_company_id'], INTEGER);
			$accountType = $this->validateParameters('user_account_type', $this->param['user_account_type'], STRING);
			$notes = $this->validateParameters('user_notes', $this->param['user_notes'], STRING , false);
            $date = date('Y-m-d h:m:i');
			try {
				// if everything checks out from user authentication then  create object of request and save
				$userObj = new User;
				$userObj->set_user_company_id($companyId);
                $userObj->set_user_branche_id($brancheId);
                $userObj->set_user_email($email);
                $userObj->set_user_password($password);
                $userObj->set_user_password_status($passwordStatus);
                $userObj->set_user_group_id($groupId);
                $userObj->set_user_account_status($accountStatus);
                $userObj->set_user_record_hide("NO");
                $userObj->set_user_block_status("UNBLOCK");
                $userObj->set_user_created_company_id($createdCompanyId);
                $userObj->set_user_created_date($date);
                $userObj->set_user_updated_company_id($createdCompanyId);
                $userObj->set_user_updated_date($date);
                $userObj->set_user_account_type($accountType);
                $userObj->set_user_notes($notes);

				if(!$userObj->insert()){
					$message = 'Failed to insert.';
				}else{
					$message = "Inserted Successfully."; 
				}

				$this->returnResponse(SUCCESS_RESPONSE,$message);
				// $user = $stmt->fetch(PDO::FETCH_ASSOC);
				// print_r($payload->userId);
			} catch (Exception $e) {
				$this->throwError(ACCESS_TOKEN_ERRORS,$e->getMessage());
			}
		}
        // update
        public function updateRecord(){
			$userId = $this->validateParameters('user_id', $this->param['user_id'], INTEGER);
			$email = $this->validateParameters('user_email', $this->param['user_email'], STRING);
			$password = $this->validateParameters('user_password', $this->param['user_password'], STRING);
            $passwordStatus = $this->validateParameters('user_password_status', $this->param['user_password_status'], STRING);
            $groupId = $this->validateParameters('user_group_id', $this->param['user_group_id'], INTEGER);
            $accountStatus = $this->validateParameters('user_account_status', $this->param['user_account_status'], STRING);
			$updatedCompanyId = $this->validateParameters('user_updated_company_id', $this->param['user_updated_company_id'], INTEGER);
			$accountType = $this->validateParameters('user_account_type', $this->param['user_account_type'], STRING);
			$notes = $this->validateParameters('user_notes', $this->param['user_notes'], STRING , false);
            $date = date('Y-m-d h:m:i');
			try {
				// if everything checks out from user authentication then  create object of request and save
				$userObj = new User;
                $userObj->set_user_id($userId);
                $userObj->set_user_email($email);
                $userObj->set_user_password($password);
                $userObj->set_user_password_status($passwordStatus);
                $userObj->set_user_group_id($groupId);
                $userObj->set_user_account_status($accountStatus);
                $userObj->set_user_updated_company_id($updatedCompanyId);
                $userObj->set_user_updated_date($date);
                $userObj->set_user_account_type($accountType);
                $userObj->set_user_notes($notes);

				if(!$userObj->update()){
					$message = 'Failed to update.';
				}else{
					$message = "Updated Successfully."; 
				}
				$this->returnResponse(SUCCESS_RESPONSE,$message);
				// $user = $stmt->fetch(PDO::FETCH_ASSOC);
				// print_r($payload->userId);
			} catch (Exception $e) {
				$this->throwError(ACCESS_TOKEN_ERRORS,$e->getMessage());
			}
		}
        // delete
        public function deleteRecord(){
            $userId = $this->validateParameters('user_id', $this->param['user_id'], INTEGER);
            $updateCompanyId = $this->validateParameters('user_updated_company_id', $this->param['user_updated_company_id'], INTEGER);
            $date = date('Y-m-d h:m:i');
			try {
				// if everything checks out from user authentication then  create object of request and save
				$userObj = new User;
                $userObj->set_user_id($userId);
                $userObj->set_user_updated_company_id($updateCompanyId);
                $userObj->set_user_updated_date($date);
                $userObj->set_user_record_hide("YES");
				if (!$userObj->delete()) {
					$message = 'Failed to delete.';
				}
				// print_r($customer);exit;
				else{
					$message = "Deleted Successfully."; 
				}
				$this->returnResponse(SUCCESS_RESPONSE,$message);
			} catch (Exception $e) {
				$this->throwError(ACCESS_TOKEN_ERRORS,$e->getMessage());
			}
        }
        // update block status
        public function blockStatus(){
			$userId = $this->validateParameters('user_id', $this->param['user_id'], INTEGER);
			$userBlockStatus = $this->validateParameters('user_block_status', $this->param['user_block_status'], STRING);
            $updateCompanyId = $this->validateParameters('user_updated_company_id', $this->param['user_updated_company_id'], INTEGER);
            $date = date('Y-m-d h:m:i');
			try {
				// if everything checks out from user authentication then  create object of request and save
				$userObj = new User;
				$userObj->set_user_id($userId);
				$userObj->set_user_block_status($userBlockStatus);
				$userObj->set_user_updated_company_id($updateCompanyId);
				$userObj->set_user_updated_date($date);
				if(!$userObj->block_status()){
					$message = 'Failed to update.';
				}else{
					$message = "Updated Successfully."; 
				}
				$this->returnResponse(SUCCESS_RESPONSE,$message);
				// $user = $stmt->fetch(PDO::FETCH_ASSOC);
				// print_r($payload->userId);
			} catch (Exception $e) {
				$this->throwError(ACCESS_TOKEN_ERRORS,$e->getMessage());
			}
		}
    }

?>